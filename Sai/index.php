<?php include('function.php'); ?>
<?php
if(isset($_POST['login'])) 
{
	$useremail = mysqli_real_escape_string($db,$_POST['useremail']);
	$userpassword = mysqli_real_escape_string($db,$_POST['userpassword']); 

	/* $sql = "SELECT * FROM sai_users WHERE email ='".$useremail."' and password='".md5($userpassword)."'";

	$result = mysqli_query($db,$sql);
	$count = mysqli_num_rows($result); */
	$user = user_login($useremail,$userpassword);
    if(!empty($user) && $user['id']) 
	{
		$_SESSION['login_user'] = $user['id'];
		$_SESSION['login_role'] = $user['role'];
		header("location: dashboard/index.php");
	}
	else 
	{
		$error = "Your Email or Password is invalid";
	}
} 
if(isset($_SESSION['login_user']))
{
	header("location: dashboard/index.php");
} 
?>
<!DOCTYPE html>  
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shri Shirdi Saibaba Sansthan Of New Zealand</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
 
</head>
<body class="hold-transition login-page">
<div class="example-modal modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Modal title</h4> -->
      </div>
      <div class="modal-body"><p>Are you want to register as Donator</p></div>
      <div class="modal-footer">
        <a href="registretion/public_register.php" target="blank"><button type="button" class="btn btn-primary pull-left">No</button></a>
        <a href="registretion/agent_register.php" target="blank"><button type="button" class="btn btn-primary">Yes</button></a>
      </div>
    </div>
  </div>
</div>
<div class="login-box">
	<div class="login-logo">
		<a href="#"> <center><img src="dist/img/logo.jpg" width="100" height="100"></center></a>
	</div>
	<div class="login-box-body">
		<form method="post">
			<div class="form-group has-feedback">
				<input type="text" class="form-control input-group" name="useremail" id="useremail" placeholder="Email" style="border-radius: 4px;" required>
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
			</div>
			<div class="form-group has-feedback">
				<input type="password" class="form-control input-group" name="userpassword" id="userpassword" placeholder="Password" style="border-radius: 4px;" required>
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>
			<div class="row">       
				<div class="col-xs-12">
					<button type="submit" name="login" class="btn btn-primary btn-block btn-flat" style="border-radius: 4px;">Sign In</button>
				</div>
			</div>
			<div class="social-auth-links text-center">
				<p> &nbsp; <p>
				<center>  <a href="forgetPassword.php" class="btn btn-block btn-google btn-flat" style="border-radius: 4px;">I forgot my password</a></center>
			</div>
			<center>
				<div style = "font-size:11px; color:#cc0000; margin-top:10px"><?php if(isset($error)){ echo $error;}  ?></div>
			</center>		 
		</form>
	</div>
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
