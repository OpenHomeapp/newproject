<?php session_start();
	 
	  $json_data = json_encode($_SESSION['array']);
	  
  $user = $_SESSION['username'];
 if(!isset($_SESSION['username']))
 {
	 header('location: ../index.php?error=your not admin..');
	 exit;
 } 
   // login data 
	 $login_data = json_decode($json_data,true);
	 $user_ID = $login_data['userID'];
     $user_token = $login_data['token'];
	 $user_type = $login_data['userType'];

	
  // end data
      $array_data = json_decode($user,true);
	  $user_name = $array_data['username'];
	  $user_password = $array_data['password'];
	  
?>
<?php

	 
          if(isset($_POST['change_pass']))
			
		{
                     
			           $data = array(
					   
					                'newPassword' => $_POST['new_password'],
									
			                        'oldPassword' => $_POST['old_password'],

									'userId' => $user_ID

							);
            
			 
			$url_send ="http://localhost/sai/api/protected/changePassword";
			   $str_data = json_encode($data);
			   
			   $ch = curl_init($url_send);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $str_data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Authorization:'.$user_token,
					'Content-Type: application/json',
					'Content-Length: ' . strlen($str_data))
				);
				curl_setopt($ch, CURLOPT_TIMEOUT, 5);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

				//execute post
				$result = curl_exec($ch);

				//close connection
				curl_close($ch);

			   $result;
			
				
				 $array_data = json_decode($result,true);
					
				 $message = $array_data['Message'];
				 
				 $array_data = json_decode($result,true);
				 
				 $Status = $array_data["Status"];
				  
         }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>OpenHome</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
  <?php
    if($user_type=="A")
	{
		$link="../dashbord/";
	}
	if($user_type=="P")
	{
		$link="../user_dashbord/";
	}
  ?>
    <a href="<?php echo $link ?>"> <center><img src="../dist/img/logo.jpg" width="100" height="100"></center></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
     

    <form method="post" action="">
        <div class="form-group has-feedback">
		 <input type="password" class="form-control" name="old_password" placeholder="Old Password"  required>
		  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
		
	  <div class="form-group has-feedback">
        <input type="password" class="form-control" name="new_password" id="reg_password" placeholder="New Password" required  pattern=".{6,}" title="Six or more characters">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
	  
		<div class="form-group has-feedback">
		<input type="password" class="form-control" name="retypepassword" placeholder="Retype password" id="reg_password2">
		<span class="glyphicon glyphicon-log-in form-control-feedback"></span>
	    </div>
			  
     
     
	 <div class="row">
      
		 <div class="col-xs-12" style="padding: 0 70px;">
		    <button type="submit" id="btnSubmit" name="change_pass" class="btn btn-warning btn-block btn-flat" style="">CHANGE PASSWORD</button>
           <!-- /.col -->
      </div>
	  
	  	 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
					  <script type="text/javascript">
						$(function () {
							$("#btnSubmit").click(function () {
								var password = $("#reg_password").val();
								var confirmPassword = $("#reg_password2").val();
								if (password != confirmPassword) {
									alert("Passwords do not match.");
									return false;
								}
								return true;
							});
						});
					</script>
		 
    </form>
	
	   
      
         

<!-- 
    <a href="register.html" class="text-center" style="color:#ffffff">Register a new membership</a> -->

  </div>
  <br>
  
      <center><font color="#FFFFFF"><?php echo @$message ;?></font></center>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
