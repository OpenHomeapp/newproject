<?php include 'header.php';
if(!isset($_SESSION['login_role']) || ($_SESSION['login_role']!=1))
{
	header("location: index.php");
}
?>

<div id="content-wrapper">
		<div class="container-fluid">

          <!-- Breadcrumbs
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Tables</li>
          </ol>-->

          <!-- DataTables Example -->
          <div class="card mb-3">
            <div class="card-header">Pooja Bookings List</div>
           <!-- <a href="excel_pooja_booking.php"><i class="fas fa-2x fa-file-excel"></i></a>-->
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Sr.No</th>
                      <th>ID</th>
					  <th>Pooja Date & Time</th>					  
                      <th>Pooja in</th>
					  <th>Devotee Name</th>                      
                      <th>Contact</th>
                      <th>Email</th>
					  <th>Address</th>
                      <th>Booked On</th>
                    </tr>
                  </thead>                 
                  <tbody>
                     <?php
				/* $where = array("role" => 3);
				if($_SESSION['login_role']==2)
				{
					$where['added_by'] = $_SESSION['login_user'];
				} */
				
				$bookings = select('sai_pooja_bookings');
				if(mysqli_num_rows($bookings))
				{	
					$i=1;
					while($row = mysqli_fetch_array($bookings))
					{ 
			  ?>		<tr>
							<td><?= $i++; ?></td>
							<td><?= $row['bookingid']; ?></td>
							<td><?= $row['pooja_date']." ".$row['pooja_time']; ?></td>
							<td><?= $row['pooja_in']; ?></td>
							<td><?= $row['devotee_name']; ?></td>
							<td><?= $row['dcontact_num']; ?></td>
							<td><?= $row['demail']; ?></td>
							<td><?= $row['address']; ?></td>
							<td><?= $row['date_booked_on']; ?></td>
							<!--<td class="center btn-group">
								<a class="btn btn-primary" href="add_booking.php?edd=<?php //echo $row['bookingid']; ?>"><i class="fas fa-pencil-alt"></i></a>
								<a class="btn btn-danger" href="?del=<?php //echo $row['userId']; ?>" onclick="return confirm('Are you sure to delete?');"><i class="fas fa-trash"></i></a>
							</td>-->
						</tr>
				<?php
					}
				}
				?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
          </div>

          <p class="small text-center text-muted my-5">
            <em>More table examples coming soon...</em>
          </p>

        </div>
        <!-- /.container-fluid -->

       <?php include('footer.php')  ?>