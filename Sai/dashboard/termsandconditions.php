<?php include('header.php');  
if(!isset($_SESSION['login_role']) || ($_SESSION['login_role']!=1))
{
	header("location: index.php");
}
$alert = 'hidden';
$message = '';
if(isset($_POST['save_terms']))
{
	$data = array(
		'hirer_name'=> $_POST['hirer'],
		'phone'=> $_POST['phone'],
		'organisation'=> $_POST['organisation'],
		'address'=> $_POST['address'],
		'naturefunction'=> $_POST['naturefunction'],		
		'hire_date'=> $_POST['hire_date'],		
		'added_by' => $_SESSION['login_user']
	);
	
	$inserted = qry_insert('sai_hirers', $data);
	if($inserted)	
	{		
		$message="Hirer successfully saved";
		$alert = "success";
		// send_mail_on_new_user_addition($inserted,$pwdd);
	}
	else	
	{
		$message="Hirer not saved. Please try again.";	
		$alert = "danger";
	}
}	
?>
<body class="hold-transition register-page">
	<div class="wrapper-sec">
		<div class="register-box-body" style="background: #fff;">
			<h3 class="login-box-msg">Terms & conditions for hire </h3>
	
	<div class="term-con">
		<ul style="list-style:none;">
			<li>
				<h3>BOOKINGS:</h3>
				<ol>
					<li>Applications will be treated in order of receipt.</li>
					<li>Bookings shall be confirmed to the hirer by SSSSNZ Inc. in writing upon receipt of advance payment. Advance payment is 50% of the Hire charges.</li>
					<li>Total payment will be required 4 weeks prior to the date of the hire. Failing to do so will result in cancelling the booking unless prior agreement with SSSSNZ Inc.</li>
					<li>Any cancellations should be made 8 weeks prior to the hire date, failing to do so will result in amount being forfeited. In case of unforeseen circumstances,  SSSSNZ Inc. can consider postponing the hire dates, but this is entirely at the discretion of SSSSNZ Inc.</li>
					<li>Hirer does not have the right to sublet facilities either partially or in whole without prior written consent from SSSSNZ Inc.</li>
					<li>SSSSNZ reserves the right to decline / cancel any bookings if the hall/ dining area is used for a different purpose apart from the description provided at the time of hiring the hall.</li>
					<li>$1000 of Bond money is payable upon acceptance of the booking which is refundable within 21 days of hire, subject to full compliance of terms and conditions.</li>
					<li>Tentative booking must be confirmed within 10 working days with advance payments and Bond; failure to do so will result in cancellation of the booking.</li>
					<li>In the event of a second hirer seeking the same dates of tentative bookings, 3 working days’ notice will be given to the person / group / organisation to confirm the booking and payment of advance; failing to do so will automatically cancel the tentative booking.</li>
					<li>15% GST payable on hire charges</li>
				</ol>
			</li>

			<li>
				<h3>TERMS OF HIRE:</h3>
				<ol>
					<li>Non Vegetarian food is not allowed in the complex and the compound.</li>
					<li>Consumption of liquor, smoking, Kava  & illicit drugs is strictly prohibited in the complex & compound.</li>
					<li>Only Water, soft drinks  allowed in the auditorium. Food not allowed.</li>
					<li>Use of lift for goods or any other items is strictly prohibited.</li>
					<li>Hirer must ensure that children are supervised at all times.</li>
					<li>Noise must be kept at acceptance levels as per the council norms</li>
					<li>Any property belonging to the SSSSNZ Inc. shall not be removed from the premises without prior consent.</li>
					<li>Any extra lighting or decorative lights must be carried out by certified technicians.</li>
					<li>Gambling, loitering, profane languages and unruly conduct is prohibited in any part of premises or the compound.</li>
					<li>The hirer agrees that the hall will not be used for any other purpose apart from the purpose as indicated at the time of hiring or is of an objectionable nature, in breach of reasonable standards of public decency or likely to create a nuisance.</li>
				</ol>
			</li>
			
			<li>
				<h3>DAMAGE / LOSS:</h3>
				<ol>
					<li>The hirer is solely responsible for any damage / loss to the property belonging to the SSSSNZ Inc.</li>
					<li>The hirer shall be solely responsible for any claims / loss arising of accident, injury or damage to persons sustained in connection with the function.</li>
				</ol>
			</li>
			
			<li>
				<h3>BOOKINGS:</h3>
				<ol>
					<li>Applications will be treated in order of receipt.</li>
					<li>Bookings shall be confirmed to the hirer by SSSSNZ Inc. in writing upon receipt of advance payment. Advance payment is 50% of the Hire charges.</li>
					<li>Total payment will be required 4 weeks prior to the date of the hire. Failing to do so will result in cancelling the booking unless prior agreement with SSSSNZ Inc.</li>
					<li>Any cancellations should be made 8 weeks prior to the hire date, failing to do so will result in amount being forfeited. In case of unforeseen circumstances,  SSSSNZ Inc. can consider postponing the hire dates, but this is entirely at the discretion of SSSSNZ Inc.</li>
					<li>Hirer does not have the right to sublet facilities either partially or in whole without prior written consent from SSSSNZ Inc.</li>
					<li>SSSSNZ reserves the right to decline / cancel any bookings if the hall/ dining area is used for a different purpose apart from the description provided at the time of hiring the hall.</li>
					<li>$1000 of Bond money is payable upon acceptance of the booking which is refundable within 21 days of hire, subject to full compliance of terms and conditions.</li>
					<li>Tentative booking must be confirmed within 10 working days with advance payments and Bond; failure to do so will result in cancellation of the booking.</li>
					<li>In the event of a second hirer seeking the same dates of tentative bookings, 3 working days’ notice will be given to the person / group / organisation to confirm the booking and payment of advance; failing to do so will automatically cancel the tentative booking.</li>
					<li>15% GST payable on hire charges</li>
				</ol>
			</li>
			
			<li>
				<h3>BOOKINGS:</h3>
				<ol>
					<li>Applications will be treated in order of receipt.</li>
					<li>Bookings shall be confirmed to the hirer by SSSSNZ Inc. in writing upon receipt of advance payment. Advance payment is 50% of the Hire charges.</li>
					<li>Total payment will be required 4 weeks prior to the date of the hire. Failing to do so will result in cancelling the booking unless prior agreement with SSSSNZ Inc.</li>
					<li>Any cancellations should be made 8 weeks prior to the hire date, failing to do so will result in amount being forfeited. In case of unforeseen circumstances,  SSSSNZ Inc. can consider postponing the hire dates, but this is entirely at the discretion of SSSSNZ Inc.</li>
					<li>Hirer does not have the right to sublet facilities either partially or in whole without prior written consent from SSSSNZ Inc.</li>
					<li>SSSSNZ reserves the right to decline / cancel any bookings if the hall/ dining area is used for a different purpose apart from the description provided at the time of hiring the hall.</li>
					<li>$1000 of Bond money is payable upon acceptance of the booking which is refundable within 21 days of hire, subject to full compliance of terms and conditions.</li>
					<li>Tentative booking must be confirmed within 10 working days with advance payments and Bond; failure to do so will result in cancellation of the booking.</li>
					<li>In the event of a second hirer seeking the same dates of tentative bookings, 3 working days’ notice will be given to the person / group / organisation to confirm the booking and payment of advance; failing to do so will automatically cancel the tentative booking.</li>
					<li>15% GST payable on hire charges</li>
				</ol>
			</li>
		</ul>
		<div style="text-align:right;">
			<p>(Signature of hirer)</p>
		</div>
	</div>
	
	
	<div class="registerboxnew">
       <h4 style="text-align:center;padding-top: 30px;">SHRI SHIRDI SAIBABA SANSTHAN OF NEW ZEALAND INCORPORATED 12-18 PRINCESS STREET, ONEHUNGA, AUCKLAND – PHONE – (09) 6366400</h4><h4 style="text-align:center;padding-bottom: 30px;"> APPLICATION FORM FOR HIRE OF SAI CENTRE</h4>
	   <div class="alert alert-<?php echo $alert; ?> alert-dismissible">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $message; ?>			  
		</div>
		<form method="post" id="termsandconditions">
			<div class="row">
				<div class="form-group has-feedback col-sm-6">
					<input type="text" name="hirer" class="form-control" placeholder="Name of Hirer" required>
				</div>
				<div class="form-group has-feedback col-sm-6">
					<input type="text" name="phone" class="form-control" placeholder="Contact phone" required>
				</div>
				<div class="form-group has-feedback col-sm-6">
					<input type="text" name="organisation" class="form-control" placeholder="Name of the Organisation" required>
				</div>
				<div class="form-group has-feedback col-sm-6">
					<input type="text" name="address" class="form-control" placeholder="Address" required>
				</div>
				<div class="form-group has-feedback col-sm-6">
					<input type="text" name="naturefunction" class="form-control" placeholder="Nature of Function" required>
				</div>
				<div class="form-group has-feedback col-sm-6">
					<input type="text" placeholder="mm/dd/yyyy" name="hire_date" class="form-control datepicker" required>
				</div>
				<div class="col-2">
					<input type="submit" class="btn btn-primary btn-block btn-flat" name="save_terms" id="save_terms" value="Register">
				</div>
			</div>	   
		</form>
		<div class="table-responsive">
			<table class="table table-bordered">
					<h3>Hire charges plus GST ( 15%)</h3>
				<tr>
					<th>Description</th>
					<th>Saturdays / Sundays/ Public Holidays </th>
					<th>Weekdays</th>
					<th>Amount Payable</th>
				</tr>
				<tr>
					<td>Auditorium / Dining / Kitchen</td>
					<td>$2,000+GST</td>
					<td>$1,000+GST</td>
					<td></td>
				</tr>
				<tr>
					<td>Hall Cleaning</td>
					<td>$200 +GST</td>
					<td>$200+GST</td>
					<td></td>
				</tr>
				<tr>
					<td>Bin Charges (depends on size)</td>
					<td>$……+GST (depends upon number of people)</td>
					<td>$200+GST</td>
					<td></td>
				</tr>
			</table>
			<table class="table table-bordered">
					<h3>WEEKDAYS ONLY:</h3>
					<p>Hourly charges (Minimum 4 hours, then hourly)</p>
				<tr>
					<th>Description</th>
					<th>Hourly charges</th>
					<th></th>
				</tr>
				<tr>
					<td>Auditorium & Kitchen / Dining</td>
					<td>$125 per hour(min four hrs+GST)</td>
					<td></td>
				</tr>
				<tr>
					<td>Auditorium</td>
					<td>$75 per hour(min four hrs+GST)</td>
					<td></td>
				</tr>
				<tr>
					<td>Kitchen / Dining</td>
					<td>$50 per hour(min four hrs) +GST</td>
					<td></td>
				</tr>
			</table>
			<table class="table table-bordered">
					<h3>WEEK ENDS AND PUBLIC HOLIDAYS:</h3>
					<p>Hourly charges (Minimum 4 hours, then hourly)</p>
				<tr>
					<td>Auditorium/Hall</td>
					<td>$150.00 (min four hrs)+GST</td>
					<td></td>
				</tr>
				<tr>
					<td>Dining( kitchen available for full day hire only)</td>
					<td>$100 per hour(min four hrs) +GST</td>
					<td></td>
				</tr>
			</table>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<form>
					<label>Deposit / Bond Paid:</label>
					<input type="text" class="borderBottom" />
					<br/>
					<label>Total Amount to be paid:</label>
					<input type="text" class="borderBottom" />
					<br/>
					<p>By completing this form the hirer acknowledges and agrees to the terms and conditions (attached) of this agreement.</p>
					<label>Name of Hirer:</label>
					<input type="text" class="borderBottom" />
					<label>Phone Number:</label>
					<input type="text" class="borderBottom" /><br/>
					<label>Date:</label>
					<input type="text" name="hire_date" placeholder="mm/dd/yyyy" class="borderBottom datepicker" />
					<label>ID: DL/Passport:</label>
					<input type="text" class="borderBottom" /><br/>
					<label>Deposit / Bond Paid:</label>
					<input type="text" class="borderBottom" />
					<label>Party Signature/ Bond return date:</label>
					<input type="text" class="borderBottom" />
					<label>Cheque no:</label>
					<input type="text" class="borderBottom" /><br/>
					
					<div style="text-align:right;">
					<input type="text" class="borderBottom"/>
					<p>(Signature of hirer)</p>
					</div>
				</form>
			</div>
		</div>
    </div>   
		</div>
	</div>
<?php include('footer.php'); ?>