<div id="wrapper">
      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
		<?php
			if(isset($_SESSION['login_role']) && $_SESSION['login_role']==1)
			{				
		?>		<li class="nav-item dropdown">
				  <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-fw fa-folder"></i>
					<span>Manage Agent</span>
				  </a>
				  <div class="dropdown-menu" aria-labelledby="pagesDropdown">
					<a class="dropdown-item" href="add_agent.php">Add Agent</a>
					<a class="dropdown-item" href="list_agent.php">List Agent</a>  
				  </div>
				</li>
	<?php 	} 
			if(isset($_SESSION['login_role']) && ($_SESSION['login_role']==1 || $_SESSION['login_role']==2))
			{
		?>		<li class="nav-item dropdown">
				  <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-fw fa-folder"></i>
					<span>Manage Donor</span>
				  </a>
				  <div class="dropdown-menu" aria-labelledby="pagesDropdown">
					<a a class="dropdown-item" href="add_donor.php">Add Donor</a>
					<a class="dropdown-item" href="list_donor.php">List Donor</a>
				 </div>
				</li>		
	<?php 	} 
			if(isset($_SESSION['login_role']) && $_SESSION['login_role']==1)
			{
	?>	        <li class="nav-item dropdown">
				  <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-fw fa-folder"></i>
					<span>Pooja Bookings</span>
				  </a>
				  <div class="dropdown-menu" aria-labelledby="pagesDropdown">
					<a class="dropdown-item" href="booking_charges.php">Booking Charges</a>
					<a class="dropdown-item" href="add_booking.php">Add Booking</a>
					<a class="dropdown-item" href="list_bookings.php">List Booking</a>
				  </div>
				</li>
	<?php 	} ?>
		<!--<li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Donor</span>
          </a>
          <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="Terms_conditions.php">Add Donor</a>
			<a class="dropdown-item" href="listdonor.php">List Donor</a>
          </div>
        </li>-->
       <li class="nav-item">
			<i class="fa fa-logout"></i>
			<a class="dropdown-item" href="termsandconditions.php">Terms & Conditions</a>
		</li>
		<li class="nav-item">
			<i class="fa fa-logout"></i>
			<a class="dropdown-item" href="logout.php">Logout</a>
		</li>
      </ul>