<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shri Shirdi Saibaba Sansthan Of New Zealand | Donator Registration</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css"
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  
<style type="text/css">
.wrapper-sec {
    max-width: 768px;
    margin: auto;
    padding: 20px;
}
.table-bordered {
    border: 1px solid #060606 !important;
}
input.borderBottom {
    border: 0;
    border-bottom: 1px solid #424141;
	box-shadow:none;
	outline:none;
}
input[type="checkbox"]{
	margin-left:15px;
}
.col-sm-12.charges {
    padding: 30px 15px;
}
  </style>
  
</head>

<body class="hold-transition register-page">
<div class="wrapper-sec">
 <!--  <div class="register-logo">
    <a href="index2.html"><b>Admin</b>LTE</a>
  </div> http://www.opennhome.com/api/register-->

  <div class="register-box-body" style="background: #fff;">
   
   
	  <a href="index.php" class="text-center">Go Back</a>
	
	<div class="registerboxnew">
       <h4 style="text-align:center;">SHRI SHIRDI SAIBABA SANSTHAN OF NEW ZEALAND INCORPORATED</h4><p style="text-align:center;">12-18 Princes St, Onehunga, Auckland Ph: 09 636 6400  - Email: secretary@shirdisaibaba.org.nz   Website: <a href="//www.shirdisaibaba.org.nz" >www.shirdisaibaba.org.nz</a>
</p>
	    <h3 class="login-box-msg">Pooja Booking Form</h3>
		<form method="post">
		<div class="row">
		  <div class="form-group has-feedback col-sm-6">
			<label>Form  No</label>
			<input type="text" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Name of the devotee:</label>
			<input type="text" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Date of the Pooja:</label>
			<input type="date" >
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Time:</label>
			<input type="time" >
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Pooja in temple:</label>
			<input type="checkbox" >
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Pooja in outs:</label>
			<input type="checkbox" >
		  </div>
		  
		  <div class="col-sm-12 charges">
		  <p>Charges as per the list(below)	$75( Pooja only )</p>
		  	<div class="col-sm-8">
				<label>Abhishek for anyone murthy (Baba ,Shiv ji,Navagraha&Balaji) $51 </label>
			</div>
			<div class="col-sm-4">
				<input type="checkbox" >
			</div>
			<div class="col-sm-8">
				<label>Any Pooja in the temple ex:Rudrabhishek,Satyanaryana Katha  $51 </label>
			</div>
			<div class="col-sm-4">
				<input type="checkbox" >
			</div>
			<div class="col-sm-8">
				<label>All Havans  ( In temple or outside) $101 </label>
			</div>
			<div class="col-sm-4">
				<input type="checkbox" >
			</div>
			<div class="col-sm-8">
				<label>Aksharabhyasa  $101 </label>
			</div>
			<div class="col-sm-4">
				<input type="checkbox" >
			</div>
			<div class="col-sm-8">
				<label>Basin Abhishek  (Every first Thursday of the Month ) $101 </label>
			</div>
			<div class="col-sm-4">
				<input type="checkbox" >
			</div>
			<div class="col-sm-8">
				<label>Engagement Ceremony  $151 </label>
			</div>
			<div class="col-sm-4">
				<input type="checkbox" >
			</div>
			<div class="col-sm-8">
				<label>KakadArathi Pooja ( for 41 days) $251 </label>
			</div>
			<div class="col-sm-4">
				<input type="checkbox" >
			</div>
			<div class="col-sm-8">
				<label>UpanayanaSanskar $251 </label>
			</div>
			
			<div class="col-sm-4">
				<input type="checkbox" >
			</div>
			<div class="col-sm-8">
				<label>Kalyanosthvam  ( only for God) $251 </label>
			</div>
			
			<div class="col-sm-4">
				<input type="checkbox" >
			</div>
			<div class="col-sm-8">
				<label>Private Marriage $251 </label>
			</div>
			<div class="col-sm-4">
				<input type="checkbox" >
			</div>
		</div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Address:</label>
			<input type="text" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Name& Time of the Pooja:</label>
			<input type="text" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Devotee Contact No</label>
			<input type="text" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>email</label>
			<input type="text" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Receipt No:</label>
			<input type="text" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Date Paid:</label>
			<input type="text" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Name of the Pandit </label>
			<input type="text" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Pooja booked by </label>
			<input type="text" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Date of the Pooja booked </label>
			<input type="text" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Signature of the of Devotee</label>
			<input type="text" class="borderBottom">
		  </div>
		  <ul style="list-style:none;">
		  	<li>Notes:
				<ul>
					<li>No Pooja booking on Thursdays</li>
					<li>No Pooja booking on festivals& temple functions </li>
					<li>Duty Pandits to perform temple Pooja </li>
					<li>Payment and the form should be completed by the devotee</li>
					<li>Poojas ( like Stayanarayakatha ) &Havans will start after 9:45 am </li>
					<li>RegularArathi& Abhishek ( Saibaba&Navagragha) will be performed as per the temple timings </li>
				</ul>
			</li>
		  </ul>
				</form>
			</div>
		</div>
    </div>
  </div>
   </div>
</div>
<!-- InputMask -->
<script src="../plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="../plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="../plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="../plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>
</body>
</html>
