<?php include('header.php'); 
if(!isset($_SESSION['login_role']) || ($_SESSION['login_role']!=1))
{
	header("location: index.php");
}
$alert = 'hidden';
$message = '';

if(isset($_POST['add_agent']))
{
	$data=array(
		'first_name'=> $_POST['firstName'],
		'last_name'=> $_POST['lastName'],
		'email'=> $_POST['emailAddress'],
		'phone'=> $_POST['phone'],		
		'gender'=> $_POST['gender'],		
		'address'=> $_POST['address'],		
		'country'=> $_POST['country'],		
		'role' => '2',
		'added_by' => $_SESSION['login_user']
	);
	if( $_POST['add_agent']=="Save")
	{
		$pwdd = randomPassword();
		$data['password']= md5($pwdd);
		if(check_user_by(array("email"=>$_POST['emailAddress'],"role"=>2)))
		{
			$message="Email '".$_POST['emailAddress']."' already exists. Please try with other email.";	
			$alert = "danger";
		}
		elseif(check_user_by(array("phone"=>$_POST['phone'],"role"=>2)))
		{
			$message = "Phone number '".$_POST['phone']."' already exists. Please try with other Phone number.";	
			$alert = "danger";
		}
		else
		{	
			$inserted = qry_insert('sai_users', $data);
			if($inserted)	
			{		
				$message="Agent successfully saved";
				$alert = "success";
				send_mail_on_new_user_addition($inserted,$pwdd);
			}
			else	
			{
				$message="Agent not saved. Please try again.";	
				$alert = "danger";
			}
		}
			
		
	}
	else if($_POST['add_agent']=="Update" && isset($_REQUEST['eda']) && $_REQUEST['eda'] > 0)
	{
		//$pwdd = randomPassword();
		//$data['password'] = md5($pwdd);
		$where = array('userId'=> $_REQUEST['eda']);
		$updated = update('sai_users', $data,$where);
		if($updated)	
		{		
			$message="Agent successfully updated";
			$alert = "success";
			//send_mail_on_new_user_addition($inserted,$pwdd);
		}
		else	
		{
			$message="Agent not updated. Please try again.";	
			$alert = "danger";
		}
	}		
} 

if(isset($_REQUEST['eda']) && $_REQUEST['eda'] > 0)
{
	$edit_agent = $_REQUEST['eda'];
	$agent = select("sai_users",array("userId" => $_REQUEST['eda']));	
	$details = mysqli_fetch_assoc($agent);	
}
?>
<style>   
@media (max-width:768px){
	.register-box {
		width:740px;
	}
	
}
@media (max-width:640px){
	.register-box {
		width:600px;
	}

}
@media (max-width:480px){
	.register-box {
		width:460px;
	}
	
}
@media (max-width: 320px){
	.register-box {
		width:300px;
	}
	
}

</style>
<body class="hold-transition register-page">
	<div class="register-box col-md-6 offset-md-2 mt-5">
		<div class="register-box-body" style="background: #fff;">
			<!--<a href="index.php" class="text-center">Go Back</a>-->
			<h3 class="login-box-msg"><?php echo (isset($_REQUEST['eda']) && $_REQUEST['eda']>0) ? 'Update' : 'Add' ?> Agent</h3>
			<div class="alert alert-<?php echo $alert; ?> alert-dismissible">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <?php echo $message; ?>			  
			</div>
			<?php //echo "<pre>"; print_r($details); echo "</pre>"; ?>
			<form method="post" id="add_agent" class="add-agent form">	
				<div class="form-group has-feedback">
					<input required type="text" class="form-control" name="firstName" id="firstName" placeholder="First Name" value="<?php echo $details['first_name']; ?>">
					<span class="glyphicon glyphicon-ok form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input required type="text" class="form-control" name="lastName" id="lastName" placeholder="Last Name" value="<?php echo $details['last_name']; ?>">
					<span class="glyphicon glyphicon-ok form-control-feedback"></span> 
				</div>
				<div class="form-group has-feedback">
					<input required type="email" class="form-control" name="emailAddress" id="emailAddress" placeholder="Email" value="<?php echo $details['email']; ?>">
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input required type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number" value="<?php echo $details['phone']; ?>">
					<span class="glyphicon glyphicon-phone form-control-feedback"></span> 
				</div>
				<div class="form-group has-feedback">
					<input required type="radio" name="gender" id="gender_m" class="gender" value="Male"<?= $details['gender']=="Male" ? ' checked' : ''; ?>><label class="label" for="gender_m">Male</label><br>
					<input required type="radio" name="gender" id="gender_f" class="gender" value="Female"<?= $details['gender']=="Female" ? ' checked' : ''; ?>><label class="label" for="gender_f">Female</label>
				</div>
				<div class="form-group has-feedback">
					<textarea required class="form-control" row="5" name="address" id="address" placeholder="Address" ><?php echo $details['address']; ?></textarea> 
				</div>
				<div class="form-group has-feedback">
				<?php $countryy = $details['country']; ?>
					<select required name="country" class="form-control">
						<option value="">Select Country</option>
						<?php
						foreach(get_countries() as $key => $value) 
						{
						?>
							<option <?= $countryy==$value ? 'selected' : $key=="NZ" && $countryy=="" ? 'selected' : ''; ?> value="<?= htmlspecialchars($value) ?>" title="<?= htmlspecialchars($value) ?>"><?= htmlspecialchars($value) ?></option>
						<?php
						}

						?>
					</select> 
				</div>				
				<div class="row">
					<div class="col-lg-3 col-md-3">
						<input type="submit" class="btn btn-primary btn-block btn-flat" name="add_agent" id="add_agent" value="<?php echo (isset($_REQUEST['eda']) && $_REQUEST['eda']>0) ? 'Update' : 'Save' ?>">
					</div>
				</div>
			</form>
		</div>   
	</div>
<?php include('footer.php'); ?>