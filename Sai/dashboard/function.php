<?php 
include('../config.php');
include('connection.php');

function randomPassword() 
{
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) 
	{
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function get_option($option)
{
	$query = select("sai_options",array("option_name" => $option ));
	$result = mysqli_fetch_array($query);
	return $result['option_value'];
}
/*******************************************
*Function for select query
*
*******************************************/
function select($table,$where="",$orderby = "",$column=FALSE)
{
	$pDatabase = DBConnect::getInstance();
    $link = $pDatabase->DBConnection();
	   
	if($column)
	{
		$sql="Select $column from ";
	}
	else
	{
		$sql="Select * from ";	
	}
	
	$sql.=$table." ";
	if(!empty($where))
	{
		$i=1;
		$sql.="where ";
		foreach($where as $key => $value)
		{
			if($i>1)
				$sql.=" and ";
			$sql.= $key."='".$value."'";
			
			$i++;			
		}
	}
	
	if(!empty($orderby))
	{
		$sql.=$orderby;	
	}
	
	/* echo "<pre>";
	echo $sql;
	echo "</pre>";  */
		
	$query=mysqli_query($link,$sql);
	
	return $query;
}

function get_results($table,$where="",$orderby = "",$column=FALSE)
{
	$all = array();
	$all_records = select($table,$where,$orderby,$column);
     // echo "resultt : ";
	if(mysqli_num_rows($all_records)) 
	{
		while($resultt = mysqli_fetch_assoc($all_records))
		{
			/*  echo "<pre>";
			 print_r($resultt);
			 echo "</pre>";  */
			$all[] = $resultt; 
		}
	}
	
	return $all;	
}
/*******************************************
*Function for insert query
*
*******************************************/
function qry_insert($table, $data){
	global $db;

    // retrieve the keys of the array (column titles)

	$fields = array_keys($data);
    $sql = "INSERT INTO ".$table."(`".implode('`,`', $fields)."`) VALUES('".implode("','", $data)."')";
	// echo "<pre>"; print_r($sql); echo "</pre>";
	$query = mysqli_query($db,$sql);

	if($query)
	{
		return mysqli_insert_id($db);
	}
	
	return 0;	
}
/*******************************************
*Function for delete query
*
*******************************************/
function truncate_table($table)
{
	$pDatabase = DBConnect::getInstance();
    $link = $pDatabase->DBConnection();
	
	$sql ="TRUNCATE TABLE ".$table;
	$query = mysqli_query($link,$sql);
	return $query;
}
function delete_record($table,$where="")
{
	$pDatabase = DBConnect::getInstance();
    $link = $pDatabase->DBConnection();
	
	$sql="Delete from ";
	$sql.=$table;
	
	if(!empty($where))
	{
		$i=1;
		$sql.=" where ";
		foreach($where as $key => $value)
		{
			if($i>1)
				$sql.=" and ";
			$sql.= $key."='".$value."'";
			
			$i++;			
		}
	}
	//echo $sql;
	$query = mysqli_query($link,$sql);
	return $query;
}

/*******************************************
*Function for update query
*
*******************************************/
function update($table,$fields,$where)
{
	$pDatabase = DBConnect::getInstance();
    $link = $pDatabase->DBConnection();
	      
	$sql = "update $table set ";   
	foreach($fields as $key => $value) 
	{         
		$fields[$key] = " `$key` = '".$value."' ";    
	}    
	
	$sql .= implode(" , ",array_values($fields));

	if(!empty($where))
	{
		$i=1;
		$sql.="where ";
		foreach($where as $key => $value)
		{
			if($i>1)
				$sql.=" and ";
			$sql.= $key."='".$value."'";
			
			$i++;			
		}
	}
	   
	$query=mysqli_query($link,$sql); 
	
	return $query;
}

function get_user_details($userid)
{
	$user= array();
	$user_details = select('sai_users', array("userId" => $userid));
	$nums=mysqli_num_rows($user_details);
	
	if($nums==1)
	{
		$user = mysqli_fetch_array($user_details);
	}
		
	 return $user;
}

function get_user_role_name($role_id)
{
	$user_role = '';
	$user_details = select('sai_user_roles', array("role_id" => $role_id));
	$nums=mysqli_num_rows($user_details);
	
	if($nums==1)
	{
		$user = mysqli_fetch_array($user_details);
		$user_role = $user['role_name'];
	}
		
	return $user_role; 
}

function check_user_by($by)
{
	$user= array();
	$user_details = select('sai_users', $by);
	$nums = mysqli_num_rows($user_details);
	
	/* if($nums==1)
	{
		$user = mysqli_fetch_array($user_details);
	} */
	
	return $nums;
}
/*******************************************
*Function for Donor register query
*
*******************************************/
function get_countries()
{
	return $countries = array("AF" => "Afghanistan",
"AX" => "Åland Islands",
"AL" => "Albania",
"DZ" => "Algeria",
"AS" => "American Samoa",
"AD" => "Andorra",
"AO" => "Angola",
"AI" => "Anguilla",
"AQ" => "Antarctica",
"AG" => "Antigua and Barbuda",
"AR" => "Argentina",
"AM" => "Armenia",
"AW" => "Aruba",
"AU" => "Australia",
"AT" => "Austria",
"AZ" => "Azerbaijan",
"BS" => "Bahamas",
"BH" => "Bahrain",
"BD" => "Bangladesh",
"BB" => "Barbados",
"BY" => "Belarus",
"BE" => "Belgium",
"BZ" => "Belize",
"BJ" => "Benin",
"BM" => "Bermuda",
"BT" => "Bhutan",
"BO" => "Bolivia",
"BA" => "Bosnia and Herzegovina",
"BW" => "Botswana",
"BV" => "Bouvet Island",
"BR" => "Brazil",
"IO" => "British Indian Ocean Territory",
"BN" => "Brunei Darussalam",
"BG" => "Bulgaria",
"BF" => "Burkina Faso",
"BI" => "Burundi",
"KH" => "Cambodia",
"CM" => "Cameroon",
"CA" => "Canada",
"CV" => "Cape Verde",
"KY" => "Cayman Islands",
"CF" => "Central African Republic",
"TD" => "Chad",
"CL" => "Chile",
"CN" => "China",
"CX" => "Christmas Island",
"CC" => "Cocos (Keeling) Islands",
"CO" => "Colombia",
"KM" => "Comoros",
"CG" => "Congo",
"CD" => "Congo, The Democratic Republic of The",
"CK" => "Cook Islands",
"CR" => "Costa Rica",
"CI" => "Cote D'ivoire",
"HR" => "Croatia",
"CU" => "Cuba",
"CY" => "Cyprus",
"CZ" => "Czech Republic",
"DK" => "Denmark",
"DJ" => "Djibouti",
"DM" => "Dominica",
"DO" => "Dominican Republic",
"EC" => "Ecuador",
"EG" => "Egypt",
"SV" => "El Salvador",
"GQ" => "Equatorial Guinea",
"ER" => "Eritrea",
"EE" => "Estonia",
"ET" => "Ethiopia",
"FK" => "Falkland Islands (Malvinas)",
"FO" => "Faroe Islands",
"FJ" => "Fiji",
"FI" => "Finland",
"FR" => "France",
"GF" => "French Guiana",
"PF" => "French Polynesia",
"TF" => "French Southern Territories",
"GA" => "Gabon",
"GM" => "Gambia",
"GE" => "Georgia",
"DE" => "Germany",
"GH" => "Ghana",
"GI" => "Gibraltar",
"GR" => "Greece",
"GL" => "Greenland",
"GD" => "Grenada",
"GP" => "Guadeloupe",
"GU" => "Guam",
"GT" => "Guatemala",
"GG" => "Guernsey",
"GN" => "Guinea",
"GW" => "Guinea-bissau",
"GY" => "Guyana",
"HT" => "Haiti",
"HM" => "Heard Island and Mcdonald Islands",
"VA" => "Holy See (Vatican City State)",
"HN" => "Honduras",
"HK" => "Hong Kong",
"HU" => "Hungary",
"IS" => "Iceland",
"IN" => "India",
"ID" => "Indonesia",
"IR" => "Iran, Islamic Republic of",
"IQ" => "Iraq",
"IE" => "Ireland",
"IM" => "Isle of Man",
"IL" => "Israel",
"IT" => "Italy",
"JM" => "Jamaica",
"JP" => "Japan",
"JE" => "Jersey",
"JO" => "Jordan",
"KZ" => "Kazakhstan",
"KE" => "Kenya",
"KI" => "Kiribati",
"KP" => "Korea, Democratic People's Republic of",
"KR" => "Korea, Republic of",
"KW" => "Kuwait",
"KG" => "Kyrgyzstan",
"LA" => "Lao People's Democratic Republic",
"LV" => "Latvia",
"LB" => "Lebanon",
"LS" => "Lesotho",
"LR" => "Liberia",
"LY" => "Libyan Arab Jamahiriya",
"LI" => "Liechtenstein",
"LT" => "Lithuania",
"LU" => "Luxembourg",
"MO" => "Macao",
"MK" => "Macedonia, The Former Yugoslav Republic of",
"MG" => "Madagascar",
"MW" => "Malawi",
"MY" => "Malaysia",
"MV" => "Maldives",
"ML" => "Mali",
"MT" => "Malta",
"MH" => "Marshall Islands",
"MQ" => "Martinique",
"MR" => "Mauritania",
"MU" => "Mauritius",
"YT" => "Mayotte",
"MX" => "Mexico",
"FM" => "Micronesia, Federated States of",
"MD" => "Moldova, Republic of",
"MC" => "Monaco",
"MN" => "Mongolia",
"ME" => "Montenegro",
"MS" => "Montserrat",
"MA" => "Morocco",
"MZ" => "Mozambique",
"MM" => "Myanmar",
"NA" => "Namibia",
"NR" => "Nauru",
"NP" => "Nepal",
"NL" => "Netherlands",
"AN" => "Netherlands Antilles",
"NC" => "New Caledonia",
"NZ" => "New Zealand",
"NI" => "Nicaragua",
"NE" => "Niger",
"NG" => "Nigeria",
"NU" => "Niue",
"NF" => "Norfolk Island",
"MP" => "Northern Mariana Islands",
"NO" => "Norway",
"OM" => "Oman",
"PK" => "Pakistan",
"PW" => "Palau",
"PS" => "Palestinian Territory, Occupied",
"PA" => "Panama",
"PG" => "Papua New Guinea",
"PY" => "Paraguay",
"PE" => "Peru",
"PH" => "Philippines",
"PN" => "Pitcairn",
"PL" => "Poland",
"PT" => "Portugal",
"PR" => "Puerto Rico",
"QA" => "Qatar",
"RE" => "Reunion",
"RO" => "Romania",
"RU" => "Russian Federation",
"RW" => "Rwanda",
"SH" => "Saint Helena",
"KN" => "Saint Kitts and Nevis",
"LC" => "Saint Lucia",
"PM" => "Saint Pierre and Miquelon",
"VC" => "Saint Vincent and The Grenadines",
"WS" => "Samoa",
"SM" => "San Marino",
"ST" => "Sao Tome and Principe",
"SA" => "Saudi Arabia",
"SN" => "Senegal",
"RS" => "Serbia",
"SC" => "Seychelles",
"SL" => "Sierra Leone",
"SG" => "Singapore",
"SK" => "Slovakia",
"SI" => "Slovenia",
"SB" => "Solomon Islands",
"SO" => "Somalia",
"ZA" => "South Africa",
"GS" => "South Georgia and The South Sandwich Islands",
"ES" => "Spain",
"LK" => "Sri Lanka",
"SD" => "Sudan",
"SR" => "Suriname",
"SJ" => "Svalbard and Jan Mayen",
"SZ" => "Swaziland",
"SE" => "Sweden",
"CH" => "Switzerland",
"SY" => "Syrian Arab Republic",
"TW" => "Taiwan, Province of China",
"TJ" => "Tajikistan",
"TZ" => "Tanzania, United Republic of",
"TH" => "Thailand",
"TL" => "Timor-leste",
"TG" => "Togo",
"TK" => "Tokelau",
"TO" => "Tonga",
"TT" => "Trinidad and Tobago",
"TN" => "Tunisia",
"TR" => "Turkey",
"TM" => "Turkmenistan",
"TC" => "Turks and Caicos Islands",
"TV" => "Tuvalu",
"UG" => "Uganda",
"UA" => "Ukraine",
"AE" => "United Arab Emirates",
"GB" => "United Kingdom",
"US" => "United States",
"UM" => "United States Minor Outlying Islands",
"UY" => "Uruguay",
"UZ" => "Uzbekistan",
"VU" => "Vanuatu",
"VE" => "Venezuela",
"VN" => "Viet Nam",
"VG" => "Virgin Islands, British",
"VI" => "Virgin Islands, U.S.",
"WF" => "Wallis and Futuna",
"EH" => "Western Sahara",
"YE" => "Yemen",
"ZM" => "Zambia",
"ZW" => "Zimbabwe");
}

/*******************************************
*Function to Send Emails
*
*******************************************/
//send_mail($userId,$payment)
function send_mail_on_new_user_addition($userId,$pwddd)
{
	$user= get_user_details($userId);
	
	if(!empty($user))
	{
		$details="<table border='1'><tr><td>Name</td><td>".$user['first_name']." ".$user['last_name']."</tr>
					<tr><td>Email</td><td>".$user['email']."</td></tr>
					<tr><td>Password</td><td>".$pwddd."</td></tr>
					<tr><td>Phone</td><td>".$user['phone']."</td></tr>
					<tr><td>Address</td><td>".$user['address']."</td></tr>					
					<tr><td>Country</td><td>".$user['country']."</td></tr></table>";
		
			// To send email to Admin to inform about new User Registeration
			
			$admin_email = get_option("admin_email"); 
			$email_subject = "Created New ".get_user_role_name($user['role']); 
			$email_body = "Hi Admin,<p> A new user has been created on ".get_option("blogname").".</p> <p>The details are:</p>".$details;
			$email_body.="<br><br><a href='".get_option('siteurl')."'><img height='100' width='100' src='".get_option('siteurl')."/images/logo.jpg'/></a><br>";
			$email_body.="<p>Contact us : ".$admin_email."</p>";
						
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
			$headers .= "From: noreply@tony-boffey.com" . "\r\n" .
			"Reply-To: noreply@tony-boffey.com" . "\r\n" .
			"X-Mailer: PHP/" . phpversion();
									
			mail($admin_email,$email_subject,$email_body,$headers, '-f'.$admin_email);

			// To send Thanks email to Registering Agent
			$art = get_user_role_name($user['role'])=="Administrator" || get_user_role_name($user['role'])=="Agent" ? 'an' : 'a'; 
			
			$user_email_subject="Greetings from ".get_option("blogname"); // Your payment is successful and
			$user_email_content="Hi ".$user['first_name']." ".$user['last_name'].",<br><p>We just want to officially welcome you with our program as ".$art." ".get_user_role_name($user['role']).".</p><p>The details are:</p>".$details."<p>If you have any questions, please feel free to contact us.</p><p>All the best</p>";
			$user_email_content.="<a href='".get_option('siteurl')."'><img height='100' width='100' src='".get_option('siteurl')."/images/logo.jpg'/></a><br>";
			$user_email_content.="<p>Contact us : ".$admin_email."</p>";
																
			mail($user['email'],$user_email_subject,$user_email_content,$headers, '-f'.$user['email']); 
		
		/************************************/
	}
}
/*****************Function to add html in email*******************/
function email_html()
{
	$html = '<!DOCTYPE HTML>
<Html lang="en">
    <head>
        <meta charset="utf-8">
			<title>INVOICE EMAILER</title>
			<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
        </head>
<body style="margin:0px auto;padding:0px;font-family:poppins;">
          <table width="100%" cellpadding="0" cellspacing="0" bgcolor="#ffbf00">
		       <tr>
			      <td align="center">
			        <table width="660" cellpadding="0" cellspacing="0" bgcolor="#f4f3f3">
					       <tr>
						         <td width="200" cellpadding="0" cellspacing="0" style="padding-left:10px;">
								       <img src="http://tony-boffey.com/Sai/images/logo.jpg" alt="#">
								  </td>
								  <td width="260" cellpadding="0" cellspacing="0">
								  </td>
								  <td width="200" cellpadding="0" cellspacing="0" align="right" style="padding-right:10px;">
								     <p>Invoice #: 123</p>
									 <p>Created: Dec 1, 2018</p>
									 <p>Due: Jan 1, 2019</p>
								  </td>
						   </tr>
					</table>
					 <table width="660" cellpadding="0" cellspacing="0" bgcolor="#f4f3f3">
					       <tr>
						         <td width="200" cellpadding="0" cellspacing="0" align="left" style="padding-left:10px;">
								     <p>Sai, Inc.</p>
									 <p>1345 lorum Road</p>
									 <p>lorum ipsum, CA 1345</p>
								  </td>
								  <td width="260" cellpadding="0" cellspacing="0">
								  </td>
								  <td width="200" cellpadding="0" cellspacing="0" align="right" style="padding-right:10px;">
								     <p>Sai Corp</p>
									 <p>John Doe</p>
									 <p><a href="#">Test@example.com</a></p>
								  </td>
						   </tr>
					</table>
					<table width="660" cellpadding="0" cellspacing="0" bgcolor="grey">
					        <tr>
								<td width="260" cellpadding="0" cellspacing="0" align="left" style="padding-left:10px;">
								    <h4 style="color:#fff;">DONATION RECEIPT</h2>
								</td>
								<td width="140" cellpadding="0" cellspacing="0">
								  </td>
								<td width="260" cellpadding="0" cellspacing="0" align="right" style="padding-right:10px;">
								    <h4 style="color:#fff;">RECEIPT NUMBER</h2>
								</td>  
							</tr>
					</table>
					<table width="660" cellpadding="0" cellspacing="0" bgcolor="#f4f3f3">
					        <tr>
								<td width="260" cellpadding="0" cellspacing="0" align="left" style="padding-left:10px;">
								    <p>Charity Name</p>
								</td>
								<td width="140" cellpadding="0" cellspacing="0">
								  </td>
								<td width="260" cellpadding="0" cellspacing="0" align="right" style="padding-right:10px;">
								    <p>Payment Date</p>
								</td>  
							</tr>
					</table>
					<table width="660" cellpadding="0" cellspacing="0" bgcolor="grey">
					        <tr>
								<td width="260" cellpadding="0" cellspacing="0" align="left" style="padding-left:10px;">
								    <h4 style="color:#fff;">DONATION RECEIPT</h2>
								</td>
								<td width="140" cellpadding="0" cellspacing="0">
								  </td>
								<td width="260" cellpadding="0" cellspacing="0" align="right" style="padding-right:10px;">
								    <h4 style="color:#fff;">RECEIPT NUMBER</h2>
								</td>  
							</tr>
					</table>
					<table width="660" cellpadding="0" cellspacing="0" bgcolor="#f4f3f3" style="border-bottom:1px solid lightgrey;">
					        <tr>
								<td width="260" cellpadding="0" cellspacing="0" align="left" style="padding-left:10px;">
								    <p>CASH</p>
								</td>
								<td width="140" cellpadding="0" cellspacing="0">
								  </td>
								<td width="260" cellpadding="0" cellspacing="0" align="right" style="padding-right:10px;">
								    <p>RS 300.00</p>
								</td>  
							</tr>
					</table>
					<table width="660" cellpadding="0" cellspacing="0" bgcolor="#f4f3f3" style="border-bottom:1px solid lightgrey;">
					        <tr>
								<td width="260" cellpadding="0" cellspacing="0" align="left" style="padding-left:10px;">
								    <p>MONEY ORDER</p>
								</td>
								<td width="140" cellpadding="0" cellspacing="0">
								  </td>
								<td width="260" cellpadding="0" cellspacing="0" align="right" style="padding-right:10px;">
								    <p>RS 75.00</p>
								</td>  
							</tr>
					</table>
					<table width="660" cellpadding="0" cellspacing="0" bgcolor="#f4f3f3" style="border-bottom:1px solid lightgrey;">
					        <tr>
								<td width="260" cellpadding="0" cellspacing="0" align="left" style="padding-left:10px;">
								    <p>CHECK</p>
								</td>
								<td width="140" cellpadding="0" cellspacing="0">
								  </td>
								<td width="260" cellpadding="0" cellspacing="0" align="right" style="padding-right:10px;">
								    <p>RS 10.00</p>
								</td>  
							</tr>
					</table>
					<table width="660" cellpadding="0" cellspacing="0" bgcolor="#f4f3f3" style="border-bottom:1px solid lightgrey;">
					        <tr>
								<td width="260" cellpadding="0" cellspacing="0" align="left" style="padding-left:10px;">
								    <p>Check Number:</p>
								</td>
								<td width="140" cellpadding="0" cellspacing="0">
								  </td>
								<td width="260" cellpadding="0" cellspacing="0" align="right" style="padding-right:10px;">
								    <p>ress</p>
								</td>  
							</tr>
					</table>
					<table width="660" cellpadding="0" cellspacing="0" bgcolor="#f4f3f3" style="border-bottom:1px solid lightgrey;">
					        <tr>
								<td width="260" cellpadding="0" cellspacing="0" align="left" style="padding-left:10px;">
								    <p></p>
								</td>
								<td width="140" cellpadding="0" cellspacing="0">
								  </td>
								<td width="260" cellpadding="0" cellspacing="0" align="right" style="padding-right:10px;">
								    <h4>Total: $385.00</h4>
								</td>  
							</tr>
					</table>
				  </td>
			   </tr>
		  </table>
</body>
</html>';
		
	return $html;
}	