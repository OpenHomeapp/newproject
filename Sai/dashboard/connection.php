<?php 
	class DBConnect
    {
		// Store the single instance of Database
		private static $m_pInstance;
		protected $connection;

		private $db_host='localhost';
		private $db_user = 'daviesis_grass';
		private $db_pass = 'artificial@123!';
		private $db_name = 'daviesis_sai1801';

		// Private constructor to limit object instantiation to within the class
		private function __construct() 
		{
			$this->connection=mysqli_connect($this->db_host,$this->db_user,$this->db_pass,$this->db_name);
			//mysql_select_db($this->db_name);
		}

		// Getter method for creating/returning the single instance of this class
		public static function getInstance()
		{
			if (!self::$m_pInstance)
			{
				self::$m_pInstance = new DBConnect();
			}
			return self::$m_pInstance;
		}

		//return connection
		public function DBConnection()
		{
			 /* echo "<pre>";
				print_r($this->connection);
				echo "</pre>";  */
		   return $this->connection;
		}
	}
?>