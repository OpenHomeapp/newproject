<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shri Shirdi Saibaba Sansthan Of New Zealand | Donator Registration</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css"
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  
  <style type="text/css">
.wrapper-sec {
    max-width: 768px;
    margin: auto;
    padding: 20px;
}
.table-bordered {
    border: 1px solid #060606 !important;
}
input.borderBottom {
    border: 0;
    border-bottom: 1px solid #424141;
	box-shadow:none;
	outline:none;
}
  </style>
  
</head>

<body class="hold-transition register-page">
<div class="wrapper-sec">
 <!--  <div class="register-logo">
    <a href="index2.html"><b>Admin</b>LTE</a>
  </div> http://localhost/sai/api/register-->

  <div class="register-box-body" style="background: #fff;">
   
    <h3 class="login-box-msg">Terms & conditions for hire </h3>
	
	<div class="term-con">
		<ul style="list-style:none;">
			<li>
				<h3>BOOKINGS:</h3>
				<ol>
					<li>Applications will be treated in order of receipt.</li>
					<li>Bookings shall be confirmed to the hirer by SSSSNZ Inc. in writing upon receipt of advance payment. Advance payment is 50% of the Hire charges.</li>
					<li>Total payment will be required 4 weeks prior to the date of the hire. Failing to do so will result in cancelling the booking unless prior agreement with SSSSNZ Inc.</li>
					<li>Any cancellations should be made 8 weeks prior to the hire date, failing to do so will result in amount being forfeited. In case of unforeseen circumstances,  SSSSNZ Inc. can consider postponing the hire dates, but this is entirely at the discretion of SSSSNZ Inc.</li>
					<li>Hirer does not have the right to sublet facilities either partially or in whole without prior written consent from SSSSNZ Inc.</li>
					<li>SSSSNZ reserves the right to decline / cancel any bookings if the hall/ dining area is used for a different purpose apart from the description provided at the time of hiring the hall.</li>
					<li>$1000 of Bond money is payable upon acceptance of the booking which is refundable within 21 days of hire, subject to full compliance of terms and conditions.</li>
					<li>Tentative booking must be confirmed within 10 working days with advance payments and Bond; failure to do so will result in cancellation of the booking.</li>
					<li>In the event of a second hirer seeking the same dates of tentative bookings, 3 working days’ notice will be given to the person / group / organisation to confirm the booking and payment of advance; failing to do so will automatically cancel the tentative booking.</li>
					<li>15% GST payable on hire charges</li>
				</ol>
			</li>

			<li>
				<h3>TERMS OF HIRE:</h3>
				<ol>
					<li>Non Vegetarian food is not allowed in the complex and the compound.</li>
					<li>Consumption of liquor, smoking, Kava  & illicit drugs is strictly prohibited in the complex & compound.</li>
					<li>Only Water, soft drinks  allowed in the auditorium. Food not allowed.</li>
					<li>Use of lift for goods or any other items is strictly prohibited.</li>
					<li>Hirer must ensure that children are supervised at all times.</li>
					<li>Noise must be kept at acceptance levels as per the council norms</li>
					<li>Any property belonging to the SSSSNZ Inc. shall not be removed from the premises without prior consent.</li>
					<li>Any extra lighting or decorative lights must be carried out by certified technicians.</li>
					<li>Gambling, loitering, profane languages and unruly conduct is prohibited in any part of premises or the compound.</li>
					<li>The hirer agrees that the hall will not be used for any other purpose apart from the purpose as indicated at the time of hiring or is of an objectionable nature, in breach of reasonable standards of public decency or likely to create a nuisance.</li>
				</ol>
			</li>
			
			<li>
				<h3>DAMAGE / LOSS:</h3>
				<ol>
					<li>The hirer is solely responsible for any damage / loss to the property belonging to the SSSSNZ Inc.</li>
					<li>The hirer shall be solely responsible for any claims / loss arising of accident, injury or damage to persons sustained in connection with the function.</li>
				</ol>
			</li>
			
			<li>
				<h3>BOOKINGS:</h3>
				<ol>
					<li>Applications will be treated in order of receipt.</li>
					<li>Bookings shall be confirmed to the hirer by SSSSNZ Inc. in writing upon receipt of advance payment. Advance payment is 50% of the Hire charges.</li>
					<li>Total payment will be required 4 weeks prior to the date of the hire. Failing to do so will result in cancelling the booking unless prior agreement with SSSSNZ Inc.</li>
					<li>Any cancellations should be made 8 weeks prior to the hire date, failing to do so will result in amount being forfeited. In case of unforeseen circumstances,  SSSSNZ Inc. can consider postponing the hire dates, but this is entirely at the discretion of SSSSNZ Inc.</li>
					<li>Hirer does not have the right to sublet facilities either partially or in whole without prior written consent from SSSSNZ Inc.</li>
					<li>SSSSNZ reserves the right to decline / cancel any bookings if the hall/ dining area is used for a different purpose apart from the description provided at the time of hiring the hall.</li>
					<li>$1000 of Bond money is payable upon acceptance of the booking which is refundable within 21 days of hire, subject to full compliance of terms and conditions.</li>
					<li>Tentative booking must be confirmed within 10 working days with advance payments and Bond; failure to do so will result in cancellation of the booking.</li>
					<li>In the event of a second hirer seeking the same dates of tentative bookings, 3 working days’ notice will be given to the person / group / organisation to confirm the booking and payment of advance; failing to do so will automatically cancel the tentative booking.</li>
					<li>15% GST payable on hire charges</li>
				</ol>
			</li>
			
			<li>
				<h3>BOOKINGS:</h3>
				<ol>
					<li>Applications will be treated in order of receipt.</li>
					<li>Bookings shall be confirmed to the hirer by SSSSNZ Inc. in writing upon receipt of advance payment. Advance payment is 50% of the Hire charges.</li>
					<li>Total payment will be required 4 weeks prior to the date of the hire. Failing to do so will result in cancelling the booking unless prior agreement with SSSSNZ Inc.</li>
					<li>Any cancellations should be made 8 weeks prior to the hire date, failing to do so will result in amount being forfeited. In case of unforeseen circumstances,  SSSSNZ Inc. can consider postponing the hire dates, but this is entirely at the discretion of SSSSNZ Inc.</li>
					<li>Hirer does not have the right to sublet facilities either partially or in whole without prior written consent from SSSSNZ Inc.</li>
					<li>SSSSNZ reserves the right to decline / cancel any bookings if the hall/ dining area is used for a different purpose apart from the description provided at the time of hiring the hall.</li>
					<li>$1000 of Bond money is payable upon acceptance of the booking which is refundable within 21 days of hire, subject to full compliance of terms and conditions.</li>
					<li>Tentative booking must be confirmed within 10 working days with advance payments and Bond; failure to do so will result in cancellation of the booking.</li>
					<li>In the event of a second hirer seeking the same dates of tentative bookings, 3 working days’ notice will be given to the person / group / organisation to confirm the booking and payment of advance; failing to do so will automatically cancel the tentative booking.</li>
					<li>15% GST payable on hire charges</li>
				</ol>
			</li>
		</ul>
		<div style="text-align:right;">
			<p>(Signature of hirer)</p>
		</div>
	</div>
	
	
	<div class="registerboxnew">
       <h4 style="text-align:center;padding-top: 30px;">SHRI SHIRDI SAIBABA SANSTHAN OF NEW ZEALAND INCORPORATED 12-18 PRINCESS STREET, ONEHUNGA, AUCKLAND – PHONE – (09) 6366400</h4><h4 style="text-align:center;padding-bottom: 30px;"> APPLICATION FORM FOR HIRE OF SAI CENTRE</h4>
		<form method="post">
		<div class="row">
		  <div class="form-group has-feedback col-sm-6">
			<input type="text" name="hirer" class="form-control" placeholder="Name of Hirer" required>
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<input type="text" name="phone" class="form-control" placeholder="Contact phone" required>
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<input type="text" name="organisation" class="form-control" placeholder="Name of the Organisation" required>
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<input type="text" name="address" class="form-control" placeholder="Address" required>
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<input type="text" name="namefunction" class="form-control" placeholder="Nature of Function" required>
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			
			<input type="date" name="data" class="form-control " required>
		  </div>
			<div class="col-xs-2"><input type="submit" class="btn btn-primary btn-block btn-flat" name="add_donor" id="add_donor" value="Register">
			 
			</div>
		  </div>
		
		  
		   <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
						  <script type="text/javascript">
							$(function () {
								$("#btnSubmit").click(function () {
									var password = $("#reg_password").val();
									var confirmPassword = $("#reg_password2").val();
									if (password != confirmPassword) {
										alert("Passwords do not match.");
										return false;
									}
									return true;
								});
							});
						</script>
		</form>
		<div class="table-responsive">
			<table class="table table-bordered">
					<h3>Hire charges plus GST ( 15%)</h3>
				<tr>
					<th>Description</th>
					<th>Saturdays / Sundays/ Public Holidays </th>
					<th>Weekdays</th>
					<th>Amount Payable</th>
				</tr>
				<tr>
					<td>Auditorium / Dining / Kitchen</td>
					<td>$2,000+GST</td>
					<td>$1,000+GST</td>
					<td></td>
				</tr>
				<tr>
					<td>Hall Cleaning</td>
					<td>$200 +GST</td>
					<td>$200+GST</td>
					<td></td>
				</tr>
				<tr>
					<td>Bin Charges (depends on size)</td>
					<td>$……+GST (depends upon number of people)</td>
					<td>$200+GST</td>
					<td></td>
				</tr>
			</table>
			<table class="table table-bordered">
					<h3>WEEKDAYS ONLY:</h3>
					<p>Hourly charges (Minimum 4 hours, then hourly)</p>
				<tr>
					<th>Description</th>
					<th>Hourly charges</th>
					<th></th>
				</tr>
				<tr>
					<td>Auditorium & Kitchen / Dining</td>
					<td>$125 per hour(min four hrs+GST)</td>
					<td></td>
				</tr>
				<tr>
					<td>Auditorium</td>
					<td>$75 per hour(min four hrs+GST)</td>
					<td></td>
				</tr>
				<tr>
					<td>Kitchen / Dining</td>
					<td>$50 per hour(min four hrs) +GST</td>
					<td></td>
				</tr>
			</table>
			<table class="table table-bordered">
					<h3>WEEK ENDS AND PUBLIC HOLIDAYS:</h3>
					<p>Hourly charges (Minimum 4 hours, then hourly)</p>
				<tr>
					<td>Auditorium/Hall</td>
					<td>$150.00 (min four hrs)+GST</td>
					<td></td>
				</tr>
				<tr>
					<td>Dining( kitchen available for full day hire only)</td>
					<td>$100 per hour(min four hrs) +GST</td>
					<td></td>
				</tr>
			</table>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<form>
					<label>Deposit / Bond Paid:</label>
					<input type="text" class="borderBottom" />
					<br/>
					<label>Total Amount to be paid:</label>
					<input type="text" class="borderBottom" />
					<br/>
					<p>By completing this form the hirer acknowledges and agrees to the terms and conditions (attached) of this agreement.</p>
					<label>Name of Hirer:</label>
					<input type="text" class="borderBottom" />
					<label>Phone Number:</label>
					<input type="text" class="borderBottom" /><br/>
					<label>Date:</label>
					<input type="text" class="borderBottom" />
					<label>ID: DL/Passport:</label>
					<input type="text" class="borderBottom" /><br/>
					<label>Deposit / Bond Paid:</label>
					<input type="text" class="borderBottom" />
					<label>Party Signature/ Bond return date:</label>
					<input type="text" class="borderBottom" />
					<label>Cheque no:</label>
					<input type="text" class="borderBottom" /><br/>
					
					<div style="text-align:right;">
					<input type="text" class="borderBottom"/>
					<p>(Signature of hirer)</p>
					</div>
				</form>
			</div>
		</div>
    </div>
  </div>
   </div>
</div>
<!-- InputMask -->
<script src="../plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="../plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="../plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="../plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>
</body>
</html>
