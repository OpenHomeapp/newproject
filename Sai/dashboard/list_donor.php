<?php include('header.php'); 
if(!isset($_SESSION['login_role']) || ($_SESSION['login_role']!=1 && $_SESSION['login_role']!=2))
{
	header("location: index.php");
}
$alert = 'hidden';
$msg = '';
if(isset($_REQUEST['del']))
{
	$delete_user = get_user_details($_REQUEST['del']);
	if(!empty($delete_user))
	{
		delete_record('sai_users',array('userId' => $_REQUEST['del']));
		$msg = "Donor '".$delete_user['first_name']." ".$delete_user['last_name']."' has been deleted.";
		$alert = "success";
	}
}
?>
  <div class="content-wrapper">
    <div class="container-fluid">
       <div class="card11 col-12 mb-3">
        <!--<div class="card-header">-->
			<div class="row my-3">
				<div class="col-2">Donors List </div>
				<div class="col-6">
					<div class="alert alert-<?php echo $alert; ?> alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<?php echo $msg; ?>			  
					</div>
				</div> 
				<div class="col-2">
				    
				    <a href="excel.php"><i class="fas fa-2x fa-file-excel"></i></a>
				    <a href="mypdf.php">PDF</a>
				</div>
				<div class="col-2 text-right">
					<a class="btn btn-primary" href="add_donor.php">Add New Donor</a>
				</div>
			</div>
		<!--</div>-->
        <div class="row">
        <div class="col-12">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
					<th>Sr.No</th>
					<th class="stateth">First Name</th>
					<th class="stateth">Last Name</th>
					<th class="stateth">Email</th>
					<th class="stateth">Phone</th>
					<th class="stateth">Gender</th>
					<th class="stateth">Address</th>
					<th class="stateth">Country</th>
					<?php if($_SESSION['login_role']==1) { ?>
					<th class="stateth">Added By</th>
					<?php } ?>
					<th>Action</th>
                </tr>
              </thead>              
              <tbody>
			  <?php
				$where = array("role" => 3);
				if($_SESSION['login_role']==2)
				{
					$where['added_by'] = $_SESSION['login_user'];
				}
				
				$donors = select('sai_users', $where);
				if(mysqli_num_rows($donors))
				{	
					$i=1;
					while($row = mysqli_fetch_array($donors))
					{ 
			  ?>		<tr>
							<td><?= $i++; ?></td>
							<td><?= $row['first_name']; ?></td>
							<td><?= $row['last_name']; ?></td>
							<td><?= $row['email']; ?></td>
							<td><?= $row['phone']; ?></td>
							<td><?= $row['gender']; ?></td>
							<td><?= $row['address']; ?></td>
							<td><?= $row['country']; ?></td>
							<?php if($_SESSION['login_role']==1) { ?>
							<td>
							<?php if($row['added_by']==$_SESSION['login_user'])
							{
								echo "You";
							}
							else
							{	
								$udetailss = get_user_details($row['added_by']);
								echo $udetailss['first_name']." ".$udetailss['last_name'];
							}
							?>
							</td>
							<?php } ?>
							<td class="center btn-group">
								<a class="btn btn-primary" href="add_donor.php?edd=<?php echo $row['userId']; ?>"><i class="fas fa-pencil-alt"></i></a>
								<a class="btn btn-danger" href="?del=<?php echo $row['userId']; ?>" onclick="return confirm('Are you sure to delete?');"><i class="fas fa-trash"></i></a>
							</td>							
						</tr>
				<?php
					}
				}
				?>
              </tbody>
			  <!--<tfoot>
                <tr>
                  <th>Sr.No</th>
				<th class="stateth">Firstname</th>
				  <th class="stateth">Lastname</th>
				  <th class="stateth">Email</th>
				  <th class="stateth">Username</th>
                  <th>Action</th>
                </tr>
              </tfoot>-->
            </table>
          </div>
        </div>
        </div>       
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
   <?php
   include 'footer.php';
   ?>