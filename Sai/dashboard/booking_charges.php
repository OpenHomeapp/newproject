<?php include('header.php'); 
if(!isset($_SESSION['login_role']) || ($_SESSION['login_role']!=1))
{
	header("location: index.php");
}
$alert = 'hidden';
$message = '';

if(isset($_POST['boooking']))
{
	truncate_table('sai_pooja_charges');
	$chargess = array_combine($_POST['charge']['des'],$_POST['charge']['cost']);
	
	/* echo "<pre> chargess==== ";
	echo print_r($chargess);
	echo "</pre>"; */
	foreach($chargess as $des => $cost)
	{
		$data = array("description"=>$des,"cost"=>$cost);
		$inserted = qry_insert('sai_pooja_charges', $data);
		if($inserted)	
		{		
			$message="Charges successfully saved";
			$alert = "success";				
		}
		else	
		{
			$message="Charges not saved. Please try again.";	
			$alert = "danger";
		}
	}  		
} 


?>
<body class="hold-transition register-page">
	<div class="register-box col-md-6 offset-md-2 mt-5">
		<div class="register-box-body" style="background: #fff;">
			<!--<a href="index.php" class="text-center">Go Back</a>-->
			<h3 class="login-box-msg">Manage Booking Charges</h3>
			<div class="alert alert-<?php echo $alert; ?> alert-dismissible">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <?php echo $message; ?>			  
			</div>
			<?php //echo "<pre>"; print_r($details); echo "</pre>"; ?>
			<form method="post" id="add_charge" class="add-charge form">
				<div class="charge-filelds-wrap">	
				<?php	$charges = get_results("sai_pooja_charges","");
				if(!empty($charges))
				{
					foreach($charges as $charge)
					{
			?>			<div class="row">
							<div class="col-sm-9">
								<div class="form-group has-feedback">
									<input required type="text" class="form-control charge-input" name="charge[des][]" placeholder="Description" value="<?php echo $charge['description']; ?>">
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group has-feedback">
									<input required min="0" type="number" class="form-control charge-input" name="charge[cost][]" placeholder="Charge" value="<?php echo $charge['cost']; ?>">
								</div>
							</div>						
					<?php	if($charge === reset($charges))
							{ 
					?>			<div class="col-sm-1">
									<input type="button" value="+" class="add" id="add" />
								</div>
					<?php 	} 	
							else
							{ 
					?>			<div class="col-sm-1">
									<input type="button" value="-" class="remove" onclick="return remove_row(this)">
								</div>
					<?php 	} 	?>
						</div>							
			<?php 	} 
				}
				else
				{
		?>			<div class="row">
						<div class="col-sm-9">
							<div class="form-group has-feedback">
								<input required type="text" class="form-control charge-input" name="charge[des][]" placeholder="Description">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group has-feedback">
								<input required min="0" type="number" class="form-control charge-input" name="charge[cost][]" placeholder="Charge">
							</div>
						</div>							
						<div class="col-sm-1">
							<input type="button" value="+" class="add" id="add" />
						</div>
					</div>
		<?php  	} 	?>	
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3">
						<input type="submit" class="btn btn-primary btn-block btn-flat" name="boooking" id="boooking" value="Save">
					</div>
				</div>
			</form>
		</div>   
	</div>
	
<?php include('footer.php'); ?>
<script>	
		jQuery("#add").click(function() 
		{
			var FieldRow = '<div class="row"><div class="col-sm-9"><div class="form-group has-feedback"><input required type="text" class="form-control charge-input" name="charge[des][]" placeholder="Description"></div></div><div class="col-sm-2"><div class="form-group has-feedback"><input required type="number" class="form-control charge-input" name="charge[cost][]" placeholder="Charge"></div></div><div class="col-sm-1"><input type="button" value="-" class="remove" onclick="return remove_row(this)"></div></div>';			
			jQuery(".charge-filelds-wrap").append(FieldRow);			    
		}); 
		
		function remove_row($thiss) 
		{
			jQuery($thiss).closest('.row').remove();
		}
	</script>