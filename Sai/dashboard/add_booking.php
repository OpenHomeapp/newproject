<?php include('header.php');  
if(!isset($_SESSION['login_role']) || ($_SESSION['login_role']!=1))
{
	header("location: index.php");
}
$alert = 'hidden';
$message = '';
if(isset($_POST['save_boooking']))
{
	$data = array(
		'devotee_name'=> $_POST['devotee_name'],
		'pooja_date'=> $_POST['date_of_pooja'],
		'pooja_time'=> $_POST['time_of_pooja'],
		'pooja_in'=> $_POST['pooja_in'],
		'pooja_charges'=> implode(",",$_POST['pooja_charges']),		
		'address'=> $_POST['daddress'],		
		'name_of_pooja'=> $_POST['nt'],		
		'dcontact_num'=> $_POST['devotee_contact'],		
		'demail'=> $_POST['devotee_email'],		
		'receipt_num'=> $_POST['receipt_num'],		
		'date_paid'=> $_POST['date_paid'],		
		'pandit_name'=> $_POST['pandit_name'],		
		'booked_by'=> $_POST['booked_by'],
		'date_booked_on'=> $_POST['booked_on'], 
		'dsignature'=> $_POST['signature'],
		'added_by' => $_SESSION['login_user']
	);
	
	$inserted = qry_insert('sai_pooja_bookings', $data);
	if($inserted)	
	{		
		$message="Booking successfully saved";
		$alert = "success";
		// send_mail_on_new_user_addition($inserted,$pwdd);
	}
	else	
	{
		$message="Booking not saved. Please try again.";	
		$alert = "danger";
	}
}	
?>
<body class="hold-transition register-page">
	<div class="wrapper-sec">
	<!-- <div class="register-logo">
		<a href="index2.html"><b>Admin</b>LTE</a>
	</div> http://www.opennhome.com/api/register-->

	<!--<div class="register-box-body" style="background: #fff;">
   	  <a href="index.php" class="text-center">Go Back</a>-->
	
	<div class="registerboxnew pooja-booking-form">
       <h4 style="text-align:center;">SHRI SHIRDI SAIBABA SANSTHAN OF NEW ZEALAND INCORPORATED</h4>
	   <p style="text-align:center;">12-18 Princes St, Onehunga, Auckland Ph: 09 636 6400  - Email: secretary@shirdisaibaba.org.nz   Website: <a href="//www.shirdisaibaba.org.nz" >www.shirdisaibaba.org.nz</a></p>
	    <h3 class="login-box-msg">Pooja Booking Form</h3>
		<div class="alert alert-<?php echo $alert; ?> alert-dismissible">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <?php echo $message; ?>			  
		</div>
		<form method="post" id="pooja_booking_form" class="pooja_booking_form form">
		<div class="row">
		  <div class="form-group has-feedback col-sm-6">
			<label>Form  No</label>
			<input required type="text" name="form_num" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Name of the devotee:</label>
			<input required type="text" name="devotee_name"class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Date of the Pooja:</label>
			<input required type="text" name="date_of_pooja" id="date_of_pooja" class="datepicker" placeholder="mm/dd/yyyy">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Time:</label>
			<input required type="text" name="time_of_pooja" id="time_of_pooja" class="timepicker">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label for="pooja_in_temp">Pooja in temple:</label>
			<input required type="radio" value="Temple" name="pooja_in" id="pooja_in_temp" class="timepicker">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label for="pooja_in_out">Pooja in outs:</label>
			<input required type="radio" value="Outs" name="pooja_in" id="pooja_in_out">
		  </div>
		  
		  <div class="col-sm-12 charges">
			<p>Charges as per the list(below) $75( Pooja only )</p>
		  	<?php	$charges = get_results("sai_pooja_charges","");
				if(!empty($charges))
				{
					foreach($charges as $charge)
					{
			?>			<div class="col-sm-8">
							<label for="charges_<?php echo $charge['id']; ?>"><?php echo $charge['description']." $".$charge['cost']; ?></label>
						</div>
						<div class="col-sm-4">
							<input type="checkbox" id="charges_<?php echo $charge['id']; ?>" name="pooja_charges[]" value="<?php echo $charge['id']; ?>">
						</div>	
		<?php 		} 
				}
		?>					
		</div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Address:</label>
			<input required type="text" name="daddress" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Name of the Pooja:</label>
			<input required type="text" name="nt" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Devotee Contact No</label>
			<input required type="text" name="devotee_contact" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Email</label>
			<input required type="email" name="devotee_email" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Receipt No:</label>
			<input required type="text" name="receipt_num" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Date Paid:</label>
			<input required type="text" name="date_paid" class="borderBottom datepicker" placeholder="mm/dd/yyyy">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Name of the Pandit </label>
			<input required type="text" name="pandit_name" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Pooja booked by </label>
			<input required type="text" name="booked_by" class="borderBottom">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Date of the Pooja booked </label>
			<input required type="text" name="booked_on" class="borderBottom datepicker" readonly placeholder="mm/dd/yyyy">
		  </div>
		  <div class="form-group has-feedback col-sm-6">
			<label>Signature of the of Devotee</label>
			<input required type="text" name="signature" Placeholder="Devotee Name" class="borderBottom">
		  </div>
		  <ul style="list-style:none;">
		  	<li>Notes:
				<ul>
					<li>No Pooja booking on Thursdays</li>
					<li>No Pooja booking on festivals& temple functions </li>
					<li>Duty Pandits to perform temple Pooja </li>
					<li>Payment and the form should be completed by the devotee</li>
					<li>Poojas ( like Stayanarayakatha ) &Havans will start after 9:45 am </li>
					<li>RegularArathi& Abhishek ( Saibaba&Navagragha) will be performed as per the temple timings </li>
				</ul>
			</li>
		  </ul>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-3">
				<input type="submit" class="btn btn-primary btn-block btn-flat" name="save_boooking" id="save_boooking" value="Submit">
			</div>
		</div>
		</form>
		</div>
    </div>
  </div>
   </div>
</div>
<?php include('footer.php'); ?>