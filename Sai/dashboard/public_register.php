<?php 
include('header.php')  ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Mobile App && Web App Development</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script type="text/javascript">
		function VoucherSourcetoPrint(source) {
			return "<html><head><script>function step1(){\n" +
					"setTimeout('step2()', 10);}\n" +
					"function step2(){window.print();window.close()}\n" +
					"</scri" + "pt></head><body onload='step1()'>\n" +
					"<img src='" + source + "' /></body></html>";
		}
		function VoucherPrint(source) {
			Pagelink = "about:blank";
			var pwa = window.open(Pagelink, "_new");
			pwa.document.open();
			pwa.document.write(VoucherSourcetoPrint(source));
			pwa.document.close();
		}
	</script>
	<style type="text/css">
		.myButton
		{
			background-color: #3c8dbc;
			border: 1px solid #3c8dbc;
			display: inline-block;
			cursor: pointer;
			color: White;
			font-family: arial;
			font-size: 10px;
			font-weight: bold;
			margin: 5px;
			padding: 5px 10px;
			text-decoration: none;
		}
	</style>
	<script type="text/javascript">
        function zoomin(){
			var myImg = document.getElementById("sky");
			var currWidth = myImg.clientWidth;
			if(currWidth == 500){
				alert("Maximum zoom-in level reached.");
			} else{
				myImg.style.width = (currWidth + 50) + "px";
			} 
		}
		function zoomout(){
			var myImg = document.getElementById("sky");
			var currWidth = myImg.clientWidth;
			if(currWidth == 50){
				alert("Maximum zoom-out level reached.");
			} else{
				myImg.style.width = (currWidth - 50) + "px";
			}
		}

   </script>
	
</head>
<body class="hold-transition register-page">
<div class="register-box">
 <!--  <div class="register-logo">
    <a href="index2.html"><b>Admin</b>LTE</a>
  </div> -->

  <div class="register-box-body" style="background: #fff;">
  <a href="../index.php" class="text-center">Go Back</a>
    <h3 class="login-box-msg">Visitor Registration</h3>
       
    <form method="post">
	
  <!---  <form role="form">--->
<!--- 	
<center>
		<table>
			<tr>
			<td  ><input type="radio" name="qrcode" value="1" class="qrraio" onclick="androidRadio()"> for Android</td>
			<td  ><div style="width:20px"></div></td>
				<td ><input type="radio" name="qrcode" value="2" class="qrraio" onclick="iosRadio()"> for Apple</td>
		
			</tr>
			<tr>
				<td><img id="sky" src="QR_Code_OpennHome.png" style="width: 100px;" alt="OpenHome QR Code" /></td>
							<td  ><div style="width:20px"></div></td>
		<td><img id="skyios" src="QR_Code_OpennHome_IOS.png" style="width: 100px;" alt="OpenHome QR Code" /></td>
				
			</tr>
		</table>
		<br />
	<button type="button" onclick="VoucherPrint('QR_Code_OpennHome.png');" class="myButton"> Print QR Code</button>
		<button type="button" onclick="zoomin()" class="myButton"> Zoom In</button>
		<button type="button" onclick="zoomout()" class="myButton"> Zoom Out</button>
		</center>--->
	
			<div class="form-group has-feedback">
			<input type="hidden" class="form-control" name="regType" value="P" >
			</div>
			
			  <div class="form-group has-feedback">
				<input type="text" class="form-control" name="firstName" id="First_name" placeholder="First Name" required>
				<span class="glyphicon glyphicon-ok form-control-feedback"></span>
			  </div>
			  <div class="form-group has-feedback">
				<input type="text" class="form-control" name="lastName" id="Last_name" placeholder="Last Name" required>
				<span class="glyphicon glyphicon-ok form-control-feedback"></span> 
			  </div>
			  <div class="form-group has-feedback">
				<input type="email" class="form-control" name="emailAddress" id="email" placeholder="Email" required>
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			  </div>
			  <div class="form-group has-feedback">
				<input type="text" class="form-control" name="cellPhone" id="cellPhone" placeholder="Cell Phone" required>
				<span class="glyphicon glyphicon-phone form-control-feedback"></span>
			  </div>
			  <div class="form-group has-feedback">
				<input type="text" class="form-control" name="username" id="UserName" placeholder="User Name" required>
				 <span class="glyphicon glyphicon-user form-control-feedback"></span> 
			  </div>
			  
			  <div class="form-group has-feedback">
				<input type="password" class="form-control" name="password" placeholder="Password" id="reg_password" required>
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			  </div>
			  
			  <div class="form-group has-feedback">
				<input type="password" class="form-control" name="retypepassword" placeholder="Retype password" id="reg_password2">
				<span class="glyphicon glyphicon-log-in form-control-feedback"></span>
			  </div>
			  
			  <div class="row">
				
				<!-- /.col -->
				<div class="col-xs-12">
				  <button type="submit" id="btnSubmit" name="register" class="btn btn-primary btn-block btn-flat">Register</button>
				</div>
				<!-- /.col -->
			  </div>
			  
	     <?php
	  
			    //echo "<h5 style='color:#083654'>".$res_data."</h5>";
		   
	 
	 ?>
	  
	   <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
					  <script type="text/javascript">
						$(function () {
							$("#btnSubmit").click(function () {
								var password = $("#reg_password").val();
								var confirmPassword = $("#reg_password2").val();
								if (password != confirmPassword) {
									alert("Passwords do not match.");
									return false;
								}
								return true;
							});
						});
					</script>
                 </form>



    
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 2.2.0 -->
<script src="../plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<script>
/*
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
  */
</script>

	<script type="text/javascript">
	QRID="sky";
        function zoomin(){
			var myImg = document.getElementById(QRID);
			var currWidth = myImg.clientWidth;
			if(currWidth == 500){
				alert("Maximum zoom-in level reached.");
			} else{
				myImg.style.width = (currWidth + 50) + "px";
			} 
		}
		function zoomout(){
			var myImg = document.getElementById(QRID);
			var currWidth = myImg.clientWidth;
			if(currWidth == 50){
				alert("Maximum zoom-out level reached.");
			} else{
				myImg.style.width = (currWidth - 50) + "px";
			}
		}
 function androidRadio(){
	 QRID="sky";
 }
 function iosRadio(){
	 QRID="skyios"
	 
 }
 /*
$(document).ready(function() {
$('.qrraio').on('change', function() {
	alert( );
	
});
});*/
   </script>
</body>
</html>
