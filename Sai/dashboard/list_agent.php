<?php include('header.php'); 
if(!isset($_SESSION['login_role']) || ($_SESSION['login_role']!=1))
{
	header("location: index.php");
}

$alert = 'hidden';
$msg = '';
if(isset($_REQUEST['del']))
{
	$delete_user = get_user_details($_REQUEST['del']);
	if(!empty($delete_user))
	{
		delete_record('sai_users',array('userId' => $_REQUEST['del']));
		$msg = "Agent '".$delete_user['first_name']." ".$delete_user['last_name']."' has been deleted.";
		$alert = "success";
	}
}
?>
  <div class="content-wrapper">
    <div class="container-fluid">
       <div class="card11 col-12 mb-3">
        <!--<div class="card-header">-->
			<div class="row my-3">
				<div class="col-3">Agents List </div>
				<div class="col-6">
					<div class="alert alert-<?php echo $alert; ?> alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<?php echo $msg; ?>			  
					</div>
				</div>
				<div class="col-3 text-right">
					<a class="btn btn-primary" href="add_agent.php">Add New Agent</a>
				</div>
			
			</div>
			<a href="excel_agentlist.php"><i class="fas fa-2x fa-file-excel"></i></a>
		<!--</div>-->
        <div class="row">
        <div class="col-12">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
					<th>Sr.No</th>
					<th class="stateth">First Name</th>
					<th class="stateth">Last Name</th>
					<th class="stateth">Email</th>
					<th class="stateth">Phone</th>
					<th class="stateth">Gender</th>
					<th class="stateth">Address</th>
					<th class="stateth">Country</th>
					<th>Action</th>
                </tr>
              </thead>              
              <tbody>
			  <?php
				$agents = select('sai_users', array("role" => 2));
								
				if(mysqli_num_rows($agents))
				{	
					$i=1;
					while($row = mysqli_fetch_array($agents))
					{ 
			  ?>		<tr>
							<td><?= $i++; ?></td>
							<td><?= $row['first_name']; ?></td>
							<td><?= $row['last_name']; ?></td>
							<td><?= $row['email']; ?></td>
							<td><?= $row['phone']; ?></td>
							<td><?= $row['gender']; ?></td>
							<td><?= $row['address']; ?></td>
							<td><?= $row['country']; ?></td>
							<td class="center btn-group">
								<a class="btn btn-primary" href="add_agent.php?eda=<?php echo $row['userId']; ?>"><i class="fas fa-pencil-alt"></i></a>
								<a class="btn btn-danger" href="?del=<?php echo $row['userId']; ?>" onclick="return confirm('Are you sure to delete?');"><i class="fas fa-trash"></i></a>
							</td>
							
						</tr>
				<?php
					}
				}
				?>
              </tbody>
			  <!--<tfoot>
                <tr>
                  <th>Sr.No</th>
				<th class="stateth">Firstname</th>
				  <th class="stateth">Lastname</th>
				  <th class="stateth">Email</th>
				  <th class="stateth">Username</th>
                  <th>Action</th>
                </tr>
              </tfoot>-->
            </table>
            
          </div>
        </div>
        </div>       
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
   <?php
   include 'footer.php';
   ?>