-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 11, 2019 at 12:52 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `daviesis_sai1801`
--

-- --------------------------------------------------------

--
-- Table structure for table `sai_hirers`
--

CREATE TABLE `sai_hirers` (
  `hirer_id` int(11) NOT NULL,
  `hirer_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `organisation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `naturefunction` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hire_date` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sai_hirers`
--

INSERT INTO `sai_hirers` (`hirer_id`, `hirer_name`, `phone`, `organisation`, `address`, `naturefunction`, `hire_date`, `added_by`, `added_date`) VALUES
(1, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:11:45'),
(2, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:15'),
(3, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:15'),
(4, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:15'),
(5, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:15'),
(6, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:16'),
(7, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:16'),
(8, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:16'),
(9, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:16'),
(10, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:16'),
(11, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:16'),
(12, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:17'),
(13, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:17'),
(14, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:17'),
(15, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:17'),
(16, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:17'),
(17, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:17'),
(18, 'Ankit Raturi', '63852741', 'ScorpSoft', 'Zirakpur', 'Testtt', '01/22/2019', 1, '2019-01-23 09:14:18');

-- --------------------------------------------------------

--
-- Table structure for table `sai_options`
--

CREATE TABLE `sai_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sai_options`
--

INSERT INTO `sai_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://tony-boffey.com/Sai/', 'yes'),
(2, 'home', 'http://tony-boffey.com/Sai/', 'yes'),
(3, 'blogname', 'Sai', 'yes'),
(4, 'blogdescription', 'Sai', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'manoj.scorpsoft@gmail.com', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `sai_pooja_bookings`
--

CREATE TABLE `sai_pooja_bookings` (
  `bookingid` int(11) NOT NULL,
  `form_num` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `devotee_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pooja_date` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `pooja_time` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pooja_in` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pooja_charges` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_of_pooja` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `dcontact_num` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `demail` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `receipt_num` int(11) NOT NULL,
  `date_paid` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pandit_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `booked_by` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `date_booked_on` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `dsignature` text COLLATE utf8_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sai_pooja_bookings`
--

INSERT INTO `sai_pooja_bookings` (`bookingid`, `form_num`, `devotee_name`, `pooja_date`, `pooja_time`, `pooja_in`, `pooja_charges`, `address`, `name_of_pooja`, `dcontact_num`, `demail`, `receipt_num`, `date_paid`, `pandit_name`, `booked_by`, `date_booked_on`, `dsignature`, `added_by`) VALUES
(1001, '0', 'Test', '01/29/2019', '08:00 AM', 'Outs', '1,2,3', 'zirakpur', 'Aksharabhyasa ', '9041725458', 'manoj.scorpsoft@gmail.com', 221, '01/21/2019', 'Ram', 'Test', '', 'signature', 1),
(1002, '0', 'Test', '01/29/2019', '08:00 AM', 'Outs', '1,2,3', 'zirakpur', 'Aksharabhyasa ', '9041725458', 'manoj.scorpsoft@gmail.com', 1231, '01/21/2019', 'Ram', 'Test', '01/21/2019', 'signature', 1),
(1003, '0', 'Test', '01/29/2019', '08:00 AM', 'Outs', '1,2,3', 'zirakpur', 'Aksharabhyasa ', '9041725458', 'manoj.scorpsoft@gmail.com', 1231, '01/21/2019', 'Ram', 'Test', '01/21/2019', 'signature', 1),
(1004, '0', 'Test', '01/29/2019', '08:00 AM', 'Outs', '1,2,3', 'zirakpur', 'Aksharabhyasa ', '9041725458', 'manoj.scorpsoft@gmail.com', 1231, '01/21/2019', 'Ram', 'Test', '01/21/2019', 'signature', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sai_pooja_charges`
--

CREATE TABLE `sai_pooja_charges` (
  `id` int(11) NOT NULL,
  `description` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `cost` int(11) NOT NULL COMMENT 'in USD($)'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sai_pooja_charges`
--

INSERT INTO `sai_pooja_charges` (`id`, `description`, `cost`) VALUES
(1, 'Abhishek for anyone murthy (Baba ,Shiv ji,Navagraha&Balaji)', 51),
(2, 'Any Pooja in the temple ex:Rudrabhishek,Satyanaryana Katha ', 51),
(3, 'All Havans ( In temple or outside)', 101),
(4, 'Aksharabhyasa', 101),
(5, 'Basin Abhishek (Every first Thursday of the Month )', 101),
(6, 'Engagement Ceremony', 151),
(7, 'KakadArathi Pooja ( for 41 days)', 251),
(8, 'UpanayanaSanskar', 251),
(9, 'Kalyanosthvam ( only for God)', 251),
(10, 'Private Marriage', 251);

-- --------------------------------------------------------

--
-- Table structure for table `sai_users`
--

CREATE TABLE `sai_users` (
  `userId` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL COMMENT '1. Admin, 2 Aagent, 3 Donor',
  `added_by` int(11) NOT NULL,
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sai_users`
--

INSERT INTO `sai_users` (`userId`, `first_name`, `last_name`, `email`, `password`, `phone`, `gender`, `address`, `country`, `role`, `added_by`, `added_date`, `updated_date`, `updated_by`) VALUES
(1, 'Manoj', 'Kamboj', 'admin', '5a1760628ea739e61d9bb798b50542d5', '9041725244', 'Male', 'Zirakpur', '', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(22, 'Manoj', 'Kumar', 'manoj.scorpsoft@gmail.com', '5f927d24e3130f3b71aa15e2f63a5728', '64611561651', 'Male', 'Zirakpur hdjxnnz', 'India', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(24, 'Ravi ', 'Kumar', 'info@newitsystems.co.nz', '42f4ac62b720c6346fa6be39e1771321', '+6427111735', 'Male', 'Level 10,34 shortland street', 'New Zealand', 2, 1, '2019-01-22 15:04:11', '0000-00-00 00:00:00', NULL),
(23, 'rituretjhjdfkg dfjkgh', 'dfgfdg', 'ankit.scorpsoft@gmail.com', '5f927d24e3130f3b71aa15e2f63a5728', '63131313', 'Male', 'sdfdrf', 'India', 3, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(27, 'simranjeet', 'singh', 'simranjeet@gmail.com', 'd668491ddfda4f2642cd272cf67e31ca', '9987654321', 'Male', 'Yamuna nagar', 'New Zealand', 3, 22, '2019-02-09 05:06:49', '0000-00-00 00:00:00', NULL),
(26, 'raman', 'saini', 'raman@anshul.com', 'f71c93364989526b0fab72b2309a4bc5', '9876543210', 'Male', 'sdsafsag', 'New Zealand', 3, 22, '2019-02-08 13:40:51', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sai_user_roles`
--

CREATE TABLE `sai_user_roles` (
  `id` int(11) NOT NULL,
  `role_id` tinyint(4) NOT NULL,
  `role_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sai_user_roles`
--

INSERT INTO `sai_user_roles` (`id`, `role_id`, `role_name`) VALUES
(1, 1, 'Administrator'),
(2, 2, 'Agent'),
(3, 3, 'Donor');

-- --------------------------------------------------------

--
-- Table structure for table `scanned_codes`
--

CREATE TABLE `scanned_codes` (
  `id` int(11) NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `scanned_info` varchar(1000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scanned_codes`
--

INSERT INTO `scanned_codes` (`id`, `agent_id`, `scanned_info`) VALUES
(1, 5, ''),
(2, 5, ''),
(3, 5, 'testing API call'),
(4, 5, 'testing API call'),
(5, 5, 'testing API call'),
(6, 5, 'testing API call'),
(7, 25, 'chandrasekhar,devlopers'),
(8, 25, 'chandrasekhar,devlopers'),
(9, 25, 'chandrasekhar,devlopers'),
(10, 25, 'chandrasekhar,devlopers'),
(11, 25, 'chandrasekhar,devlopers'),
(12, 25, 'chandrasekhar,devlopers'),
(13, 7, 'ios'),
(14, 7, 'ios'),
(15, 7, 'ios'),
(16, 7, 'ios'),
(17, 7, 'ios'),
(18, 7, 'ios'),
(19, 7, 'ios'),
(20, 7, 'ios develper,qrcode.'),
(21, 7, 'ios develper,qrcode.'),
(22, 7, 'ios develper,qrcode.'),
(23, 7, 'chandrasekhar iosdev'),
(24, 7, 'chandrasekhar iosdev'),
(25, 7, 'chandrasekhar iosdev'),
(26, 7, 'chandrasekhar iosdev'),
(27, 7, 'chandrasekhar iosdev'),
(28, 7, 'chandrasekhar iosdev'),
(29, 7, 'chandrasekhar iosdev'),
(30, 7, 'chandrasekhar iosdev'),
(31, 7, 'chandrasekhar iosdev'),
(32, 7, 'chandrasekhar iosdev'),
(33, 7, 'chandrasekhar iosdev'),
(34, 7, 'chandrasekhar iosdev'),
(35, 7, 'chandrasekhar iosdev'),
(36, 7, 'chandrasekhar iosdev'),
(37, 7, 'chandrasekhar iosdev'),
(38, 7, 'chandrasekhar iosdev'),
(39, 7, 'chandrasekhar iosdev'),
(40, 7, 'chandrasekhar iosdev'),
(41, 7, 'chandrasekhar iosdev'),
(42, 7, 'chandrasekhar iosdev'),
(43, 7, 'chandrasekhar iosdev'),
(44, 7, 'chandrasekhar iosdev'),
(45, 7, 'chandrasekhar iosdev'),
(46, 7, 'chandrasekhar iosdev'),
(47, 7, 'chandrasekhar iosdev'),
(48, 7, 'chandrasekhar iosdev'),
(49, 7, 'chandrasekhar iosdev'),
(50, 7, 'chandrasekhar iosdev'),
(51, 7, 'chandrasekhar iosdev'),
(52, 7, 'chandrasekhar iosdev'),
(53, 7, 'chandrasekhar iosdev'),
(54, 7, 'chandrasekhar iosdev'),
(55, 7, 'chandrasekhar iosdev'),
(56, 7, 'chandrasekhar iosdev'),
(57, 7, 'chandrasekhar iosdev'),
(58, 7, 'chandrasekhar iosdev'),
(59, 7, 'chandrasekhar iosdev'),
(60, 7, 'chandrasekhar iosdev'),
(61, 7, 'chandrasekhar iosdev'),
(62, 7, 'chandrasekhar iosdev'),
(63, 7, 'chandrasekhar iosdev'),
(64, 7, 'chandrasekhar iosdev'),
(65, 7, 'chandrasekhar iosdev'),
(66, 7, 'chandrasekhar iosdev'),
(67, 7, 'chandrasekhar iosdev'),
(68, 7, 'chandrasekhar iosdev'),
(69, 7, 'chandrasekhar iosdev'),
(70, 7, 'chandrasekhar iosdev'),
(71, 7, 'chandrasekhar iosdev'),
(72, 7, 'chandrasekhar iosdev'),
(73, 7, 'chandrasekhar iosdev'),
(74, 7, 'chandrasekhar iosdev'),
(75, 7, 'chandrasekhar iosdev'),
(76, 7, 'chandrasekhar iosdev'),
(77, 7, 'chandrasekhar iosdev'),
(78, 7, 'chandrasekhar iosdev'),
(79, 7, 'chandrasekhar iosdev'),
(80, 7, 'chandrasekhar iosdev'),
(81, 7, 'chandrasekhar iosdev'),
(82, 7, 'chandrasekhar iosdev'),
(83, 7, 'chandrasekhar iosdev'),
(84, 7, 'chandrasekhar iosdev'),
(85, 7, 'chandrasekhar iosdev'),
(86, 7, 'chandrasekhar iosdev'),
(87, 7, 'chandrasekhar iosdev'),
(88, 7, 'chandrasekhar iosdev'),
(89, 7, 'chandrasekhar iosdev'),
(90, 7, 'chandrasekhar iosdev'),
(91, 7, 'ios qr code done'),
(92, 7, 'ios qr code done'),
(93, 7, 'ios qr code done'),
(94, 7, 'ios qr code done'),
(95, 7, 'ios qr code done'),
(96, 7, 'ios qr code done'),
(97, 7, 'ios qr code done'),
(98, 7, 'Licence Number - 123456, Licence Type - Individual, Licence Class - Agent, Expiry Date - 10082018, Agency - agency, Branch - auckland'),
(99, 7, 'Licence Number - 123456, Licence Type - Individual, Licence Class - Agent, Expiry Date - 10082018, Agency - agency, Branch - auckland'),
(100, 7, 'Licence Number - 123456, Licence Type - Individual, Licence Class - Agent, Expiry Date - 10082018, Agency - agency, Branch - auckland'),
(101, 5, 'chandraekhar kunchala'),
(102, 5, 'chandraekhar kunchala'),
(103, 7, 'chandraekhar kunchala'),
(104, 7, 'chandraekhar kunchala'),
(105, 5, NULL),
(106, 5, NULL),
(107, 5, 'http://en.m.wikipedia.org'),
(108, 5, 'big SALE');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sai_hirers`
--
ALTER TABLE `sai_hirers`
  ADD PRIMARY KEY (`hirer_id`);

--
-- Indexes for table `sai_options`
--
ALTER TABLE `sai_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `sai_pooja_bookings`
--
ALTER TABLE `sai_pooja_bookings`
  ADD PRIMARY KEY (`bookingid`);

--
-- Indexes for table `sai_pooja_charges`
--
ALTER TABLE `sai_pooja_charges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sai_users`
--
ALTER TABLE `sai_users`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `sai_user_roles`
--
ALTER TABLE `sai_user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scanned_codes`
--
ALTER TABLE `scanned_codes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sai_hirers`
--
ALTER TABLE `sai_hirers`
  MODIFY `hirer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `sai_options`
--
ALTER TABLE `sai_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=580;

--
-- AUTO_INCREMENT for table `sai_pooja_bookings`
--
ALTER TABLE `sai_pooja_bookings`
  MODIFY `bookingid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1005;

--
-- AUTO_INCREMENT for table `sai_pooja_charges`
--
ALTER TABLE `sai_pooja_charges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sai_users`
--
ALTER TABLE `sai_users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `sai_user_roles`
--
ALTER TABLE `sai_user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scanned_codes`
--
ALTER TABLE `scanned_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
